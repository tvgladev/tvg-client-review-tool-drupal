<?php

$_VHOST = getvhost();

function getvhost()
{
    $vhost = array();
    foreach ($_SERVER as $key => $val) {
        if (!preg_match('/REDIRECT/', $key) &&
            preg_match('/(BASE_URL|DB_HOST|DB_NAME|DB_USER|DB_PASS|REDIS_HOST)/', $key)) {
            $vhost[$key] = $val;
        }
    }
    return $vhost;
}