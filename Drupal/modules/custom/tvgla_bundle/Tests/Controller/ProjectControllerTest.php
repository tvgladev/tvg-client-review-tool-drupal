<?php

namespace Drupal\tvgla_bundle\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Entity\EntityManager;

/**
 * Provides automated tests for the tvgla_bundle module.
 */
class ProjectControllerTest extends WebTestBase {


  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  protected $entity_manager;
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "tvgla_bundle ProjectController's controller functionality",
      'description' => 'Test Unit for module tvgla_bundle and controller ProjectController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests tvgla_bundle functionality.
   */
  public function testProjectController() {
    // Check that the basic functions of module tvgla_bundle.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
