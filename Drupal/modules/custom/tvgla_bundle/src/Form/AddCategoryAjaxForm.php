<?php

namespace Drupal\tvgla_bundle\Form;


use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;


/**
 * Created by PhpStorm.
 * User: messified
 * Date: 7/6/16
 * Time: 11:53 AM
 */
class AddCategoryAjaxForm extends FormBase{


	public function getFormId() {
		return 'ajax_form';
	}

	public function buildForm(array $form, FormStateInterface $form_state) {

		$request = $this->getRequest();
		$nid = $request->attributes->get('nid');
		$node = $this->load($nid);

		$form['title'] = [
			'#type' => 'textfield',
			'#title' => 'Title',
			'#description' => 'Please enter in a Category title',
		];

		$form['field_project'] = [
			'#type' => 'select',
			'#title' => $this->t('Project'),
			'#options' => [
				$nid => $nid
			],
		];

		$form['actions']['#type'] = 'actions';
		$form['actions']['submit'] = array(
			'#type' => 'submit',
			'#value' => $this->t('Submit'),
			'#button_type' => 'primary',
			'#weight' => 10,
			'#ajax' => [
				'callback' => array($this, 'apiClient'),
				'event' => 'change',
				'progress' => array(
					'type' => 'throbber',
				),
			]
		);

		return $form;
	}

	public function submitForm(array &$form, FormStateInterface $form_state) {
		$commands[] = array('command' => 'reloadPage');
		return array('#type' => 'ajax', '#commands' => $commands);
	}

	public function apiClient() {

		$serialized_entity = json_encode([
			'title' => [['value' => 'Test Category']],
			'type' => [['target_id' => 'tvgla_category']],
			'field_project' => [['value' => 13]],
		]);

		$uri = 'http://dev.reviewtool.tvg.la/entity/node?_format=json';
		$response = \Drupal::httpClient()->post($uri, [
			'body' => $serialized_entity,
			'headers' => [
					'Content-Type' => 'application/json',
					'X-CSRF-Token' => $this->getRequest()->getSession()->get('token')
			]
		]);

		return $response;
	}

	public function load($id) {
		$node = node_load($id);

		return $node;
	}


}