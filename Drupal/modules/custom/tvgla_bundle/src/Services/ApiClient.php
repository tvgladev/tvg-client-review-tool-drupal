<?php

namespace Drupal\tvgla_bundle\Services;

use GuzzleHttp\Client;

class ApiClient {

	/**
	 * @var $client Client
	 */
	private $client;

	public function __construct()
	{
		$this->client = new Client([
			'base_uri' => 'http://review.tvg.la/',
		]);
	}

	public function getProjects()
	{
		return json_decode($this->client
			->request('GET', 'api/test/1')
			->getBody()
		);
	}
}
