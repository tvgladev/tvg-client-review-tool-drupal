<?php

namespace Drupal\tvgla_bundle\Unit;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EditUnit.
 *
 * @package Drupal\tvgla_bundle\Unit
 */
class EditUnit extends ControllerBase {

  /**
	 * @param int $nid
	 *
	 * @return string
	 */
  public function approveUnit(Request $request) {

    $projectIds = explode(';', $request->query->get('projectIds'));

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    foreach ($projectIds as $projectId) {
      // echo "Project ID '$projectId' \n";

      $node = $node_storage->load($projectId);

      if (!empty($node)) {
        // echo "Project ID exists: $projectId \n";
        $node->field_approved = true;
        
        $node->save(); 
      } else {
        return new AjaxResponse(['success' => FALSE]);
        // echo "Project ID doesn't exist: $projectId \n";
      }
      
    }

    return new AjaxResponse(['success' => TRUE]);
  }

  /**
	 * @param int $nid
	 *
	 * @return string
	 */
  public function reviewUnit(Request $request) {

    $projectIds = explode(';', $request->query->get('projectIds'));

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    foreach ($projectIds as $projectId) {
      // echo "Project ID '$projectId' \n";

      $node = $node_storage->load($projectId);

      if (!empty($node)) {
        // echo "Project ID exists: $projectId \n";
        $node->field_approved = false;
        
        $node->save(); 
      } else {
        return new AjaxResponse(['success' => FALSE]);
        // echo "Project ID doesn't exist: $projectId \n";
      }
      
    }

    return new AjaxResponse(['success' => TRUE]);
  }

}