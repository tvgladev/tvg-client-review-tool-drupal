<?php

namespace Drupal\tvgla_bundle\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\tvgla_bundle\Services\ApiClient;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ProjectController.
 *
 * @package Drupal\tvgla_bundle\Controller
 */
class ProjectController extends ControllerBase {

	/**
	 * @var $entity_manager EntityManager
	 */
	protected $entity_manager;

	/**
	 * @var $request RequestStack
	 */
	protected $request;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(EntityManager $entity_manager, RequestStack $request) {
		$this->entity_manager = $entity_manager;
		$this->request = $request;
	}

	/**
	 * {@inheritdoc}col-sm-12
	 */
	public static function create(ContainerInterface $container) {
		return new static(
			$container->get('entity.manager'),
			$container->get('request_stack')
		);
	}

	/**
	 * @param int $node
	 *
	 * @return string
	 */
	public function indexAction($node = null) {

		$client = $this->getClient();
		$projects = $client->getProjects();

		return [
			'#markup' => 'Content Here',
		];
	}

	private function getClient()
	{
		return new ApiClient();
	}
}
