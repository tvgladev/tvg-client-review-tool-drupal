<?php

namespace Drupal\tvgla_bundle\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
* Provides a 'ProjectMenuBlock' block.
*
* @Block(
*  id = "project_menu_block",
*  admin_label = @Translation("Project menu block"),
* )
*/
class ProjectMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

	/**
	 * Symfony\Component\HttpFoundation\Request definition.
	 *
	 * @var RequestStack
	 */
	protected $request;

	/**
	 * @var EntityManager
	 */
	protected $entityManager;

	/**
	 * Construct.
	 *
	 * @param array $configuration
	 *   A configuration array containing information about the plugin instance.
	 * @param string $plugin_id
	 *   The plugin_id for the plugin instance.
	 * @param string $plugin_definition
	 *   The plugin implementation definition.
	 */
	public function __construct(
		array $configuration,
		$plugin_id,
		$plugin_definition,
		RequestStack $request,
		EntityManager $entityManager
	) {
		parent::__construct($configuration, $plugin_id, $plugin_definition);
		$this->request = $request;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
		return new static(
			$configuration,
			$plugin_id,
			$plugin_definition,
			$container->get('request_stack'),
			$container->get('entity.manager')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function build() {

		$build = [];
		$option = [];
		$nid = $this->getCurrentNodeId();
		if (null !== $nid) {
			$option = [
				'query' => ['project' => $nid],
			];
		}

		$build[ 'project_menu_block' ]['add_category_link'] = array(
			'#type' => 'link',
			'#title' => $this->t('Add Category'),
			'#url' => Url::fromRoute('node.add', ['node_type' => 'tvgla_category'], $option),
			'#cache' => [
				'contexts' => [
					'url.path',
				]
			],
			'#attributes' => [
				'class' => ['use-ajax btn btn-default project-btn add-category'],
				'data-dialog-type' => 'modal',
				'data-dialog-options' => Json::encode(
					[
						'width' => '80%',
						'height', 700
					]
				),
			],
		);

		$build[ 'project_menu_block' ]['add_asset_link'] = array(
			'#type' => 'link',
			'#title' => $this->t('Add Asset'),
			'#url' => Url::fromRoute('node.add', ['node_type' => 'tvgla_asset'], $option),
			'#cache' => [
				'contexts' => [
					'url.path',
				]
			],
			'#attributes' => [
				'class' => ['use-ajax btn btn-default project-btn add-asset'],
				'data-dialog-type' => 'modal',
				'data-dialog-options' => Json::encode(
					[
						'width' => '80%',
						'height', 700
					]
				),
			],
		);

		return $build;
	}


	/**
	 * @return null|array
	 */
	private function getCurrentNodeId() {
		$currentRequest = $this->request->getCurrentRequest();
		$node = $currentRequest->get('node');

		if (null === $node) {
			return null;
		}

		return $node->Id();
	}
}
