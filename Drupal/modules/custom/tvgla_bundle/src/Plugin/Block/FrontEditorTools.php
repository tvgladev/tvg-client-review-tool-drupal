<?php

namespace Drupal\tvgla_bundle\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'FrontEditorTools' block.
 *
 * @Block(
 *  id = "front_editor_tools",
 *  admin_label = @Translation("Front editor tools"),
 * )
 */
class FrontEditorTools extends BlockBase implements ContainerFactoryPluginInterface {

	/**
	 * Symfony\Component\HttpFoundation\RequestStack definition.
	 *
	 * @var $requestStack RequestStack
	 */
	protected $request;
	/**
	 * Drupal\Core\Entity\EntityManager definition.
	 *
	 * @var $entityManager EntityManager
	 */
	protected $entityManager;

	/**
	 * Construct.
	 *
	 * @param array $configuration
	 *   A configuration array containing information about the plugin instance.
	 * @param string $plugin_id
	 *   The plugin_id for the plugin instance.
	 * @param string $plugin_definition
	 *   The plugin implementation definition.
	 */
	public function __construct(
		array $configuration,
		$plugin_id,
		$plugin_definition,
		RequestStack $request,
		EntityManager $entity_manager
	) {
		parent::__construct($configuration, $plugin_id, $plugin_definition);
		$this->requestStack = $request;
		$this->entityManager = $entity_manager;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
		return new static(
			$configuration,
			$plugin_id,
			$plugin_definition,
			$container->get('request_stack'),
			$container->get('entity.manager')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function build() {
		$build = [];

		$build[ 'front_editor_tools' ][ 'add_project_link' ] = array(
			'#type'       => 'link',
			'#title'      => $this->t('Add Project'),
			'#url'        => Url::fromRoute('node.add', ['node_type' => 'tvgla_project']),
			'#cache'      => [
				'contexts' => [
					'url.path',
				]
			],
			'#attributes' => [
				'class'               => ['use-ajax btn btn-default project-btn add-project'],
				'data-dialog-type'    => 'modal',
				'data-dialog-options' => Json::encode(
					[
						'width' => '80%',
						'height',
						700
					]
				),
			],
		);

		$build[ 'front_editor_tools' ][ 'add_client_link' ] = array(
			'#type'       => 'link',
			'#title'      => $this->t('Add Client'),
			'#url'        => Url::fromRoute('node.add', ['node_type' => 'tvgla_client']),
			'#cache'      => [
				'contexts' => [
					'url.path',
				]
			],
			'#attributes' => [
				'class'               => ['use-ajax btn btn-default project-btn add-client'],
				'data-dialog-type'    => 'modal',
				'data-dialog-options' => Json::encode(
					[
						'width' => '80%',
						'height',
						700
					]
				),
			],
		);

		return $build;
	}

	/**
	 * @return null|array
	 */
	private function getCurrentNodeId() {
		$currentRequest = $this->request->getCurrentRequest();
		$node = $currentRequest->get('node');

		if (NULL === $node) {
			return NULL;
		}

		return $node->Id();
	}

}
