(function ($, Drupal, Bootstrap) {

  // Global variables
var path				= window.location.pathname.split('/'),
  root				= window.location.origin,
      currentClient       = path[2],
  endPoints			= {
    clientList      : root	+ '/api/clients',
          projectsList    : root  + '/api/projects/{0}/{1}', // {0} = Group ID, {1} = Campaign ID
          assetsList      : root  + '/api/assets/{0}/{1}', // {0} = Group ID, {1} = Campaign ID
          assetContent    : root  + '/api/content/{0}' // {0} = Node ID
  },
      urls                = {
          addAssetround   : '/group/{0}/content/create/group_node:asset_round?edit[field_project]={1}&edit[field_round]={2}&group={0}&campaign={3}&destination={4}', // {0} = Group ID, {1} = Project ID, {2} = Round Number, {3} = Campaign ID, {4} = Destination URI
          editNode        : '/node/{0}/edit?group={1}&campaign={2}&destination={3}' // {0} = Node ID, {1} = Group ID, {2} = Destination URI
      };
      
  var addCampaign = $('#add-campaign-btn');
  var editCampaign = $('#edit-campaign-btn');
  var addProject = $('#add-project-btn');
  var editProject = $('#edit-project-btn');
  var addAssetround = $('#add-asset-round-btn');
  var editAssetround = $('#edit-asset-round-btn');
  var editClient = $('#edit-client-btn');

  var isAdministrator = !!$('#toolbar-administration').length;
  var onReview = true;
  var selectedOnReview = new Set();
  var selectedOnApproved = new Set();

  // Extract the Group ID and Campaign ID from the page
  var group = $('#group_id'),
      group_id = group[0] ? group[0].innerText : null,
      campaign = $('#campaign_id'),
      campaign_id = campaign[0] ? campaign[0].innerText : null;
      
  loginError();
  
  // Entry
  $(function(){
      var pathFrontPage = $('.path-frontpage');
      var pathClientPage = $('.path-group #block-client-campaigns-grid');
      var directAssetPage = $('.page-node-type-asset-round');
      var userLoggedIn = $('.user-logged-in');
      
      if(directAssetPage.length > 0) {
          buildVideoUploadPlayers();
      }
      
      if(userLoggedIn.length > 0) {
          if(pathFrontPage.length > 0 || pathClientPage.length > 0) {
              callAPI(endPoints.clientList, {}, 'GET', 'JSON', true, buildClientFilter);
          } else {
              callAPI(endPoints.projectsList.format(group_id, campaign_id), {}, 'GET', 'JSON', false, getAssetProjects);
              callAPI(endPoints.assetsList.format(group_id, campaign_id), {}, 'GET', 'JSON', false, getAssetProjectRounds);
              buildSideBar();
          }
          
          addCampaign.attr('href', addCampaign.attr('href') + "?destination="+path.join('/'));
          editCampaign.attr('href', editCampaign.attr('href') + "?destination="+path.join('/'));
          addProject.attr('href', addProject.attr('href') + "&destination="+path.join('/'));
          editClient.attr('href', editClient.attr('href') + "?destination="+path.join('/'));
          rebuildButtons();
      }
      else {
          var logForm = $('.user-login-form');
          if (logForm) {
              var formAct = logForm.attr('action');
              if (formAct != '' && formAct != undefined) {
                  formAct = formAct.split('?destination=');
                  logForm.attr('action', formAct[0] + "?destination=" + path.join('/'));
              }
          }
      }
      
      var modalTitle = "";
      // Modal Trigger Buttons
      $('.use-ajax').on('click', function(){
          var btnID = $(this).attr('id');
          modalTitle = btnID == 'edit-campaign-btn' || btnID == 'edit-project-btn' ? null : $(this).text();
      });
      // Modal appears
      var observer = new MutationObserver(function(mutations){
          mutations.forEach(function(mutation) {
              if($(mutation.target).hasClass('modal-open')) {
                  if(modalTitle != null) {
                      $(mutation.target).find('.modal-title').html(modalTitle);
                  }
                  
                  // TODO: This should not be done here. This is a dom hack
                  $('.field--name-field-archive label').html($('.field--name-field-archive label input'));
                  $('.field--name-field-archive label').append("ARCHIVE");
                  $('#drupal-modal .form-actions').prepend($('.field--name-field-archive label'));
                  
                  $('.view-group-members button').remove();
                  $('.view-group-members .dropdown-menu a').once().each(function(){
                      $(this).attr('href', $(this).attr('href').replace(/destination=.*/, 'destination=' + path.join('/')));
                      $(this).closest('div').html(
                          $(this).attr('class', 'use-ajax btn btn-danger')
                          .attr('data-dialog-type', 'modal')
                      );
                  });
                  Drupal.attachBehaviors(document, window.drupalSettings);
              }
          });
      });
      observer.observe(document.body, { attributes: true, childList: true, characterData: true });
  });
  
/**
 * centralized API call function
 * @arg  {String}    url       URL of endpoint to hit
 * @arg  {Object}    data      data to pass to api if any
 * @arg  {String}    method    call method GET|POST etc.
 * @arg	 {string}    type      data type
 * @arg  {Function}  callback  Success function
 */	
function callAPI(url,data,method,type,asyncronous,callback) {
  var self = this;
  $.ajax({
    url: url,
    data: data,
    method: method,
    dataType: type,
    async: asyncronous,
    success:function(data) {
      var response = {
        status	: 'ok',
        data	: data
      };
  
      callback(response);
    },
    error:function(jqXHR, textStatus, errorThrown){
      var response = {
        status : 'error',
        errors : [{
          code : jqXHR.status,
          message : errorThrown || textStatus
        }],
        data : null
      };
      
      callback(response);
    }
  });
}
  
/**
 * receives JSON data from API and builds client list for filtering
 * @arg  {object}  data  ajax response from /api/clients/
 */
function buildClientFilter(data) {
  var clientDropdown = $('#client-dropdown');
  if(data.status === 'ok' && clientDropdown) {
    data = data.data;
  
    $('#client-dropdown').append(
      '<button class="btn btn-warning project-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
      '<span class="caret"></span>' +
      '<span class="client-dropdown-selected">Client Filter</span>' +
      '</button>' +
      '<ul class="client-dropdown dropdown-menu" aria-labelledby="dropdownMenu1"></ul>'
          );
    $('#client-dropdown').show();
    $('#client-dropdown ul').append('<li><a href="/">All</a></li>');
          var selectedClientValue = "Client Filter";
          var selectedClientName = "Client Filter"
    $(data).each(function (i,v) {
      var client = v.label;
              var clientUri = client.replace(/ /g, '-');
              var clientLink = v.link;
              
              var regMatch = new RegExp('.*"\/clients\/'+currentClient+'".*', 'g');
              var found = clientLink.match(regMatch);
              selectedClientValue = (found) ? clientUri: selectedClientValue;
              selectedClientName = (found) ? client: selectedClientName;
              
      $('#client-dropdown ul').append('<li>'+clientLink+'</li>');
    });
          $('#client-dropdown .btn').val(selectedClientValue);
    $('#client-dropdown .btn').html('<span class="caret"></span><span class="client-dropdown-selected">' + selectedClientName + '</span>');
  }
}
  
  function rebuildButtons() {
      if($('.use-ajax').length) {
          for(var i = 0; i < Drupal.ajax.instances.length; i++) {
              var instance = Drupal.ajax.instances[i];
              switch(instance.selector) {
                  case "#add-campaign-btn":
                      instance.url = addCampaign.attr('href');
                      instance.element_settings.url = instance.url;
                      instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                      break;
                  case "#edit-campaign-btn":
                      instance.url = editCampaign.attr('href');
                      instance.element_settings.url = instance.url;
                      instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                      break;
                  case "#add-project-btn":
                      instance.url = addProject.attr('href');
                      instance.element_settings.url = instance.url;
                      instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                      break;
                  case "#edit-project-btn":
                      instance.url = editProject.attr('href');
                      instance.element_settings.url = instance.url;
                      instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                      break;
                  case "#add-asset-round-btn":
                      instance.url = addAssetround.attr('href');
                      instance.element_settings.url = instance.url;
                      instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                      break;
                  case "#edit-asset-round-btn":
                      instance.url = editAssetround.attr('href');
                      instance.element_settings.url = instance.url;
                      instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                      break;
              }
          }
      }
  }
  
  var assetProjects = {};
  function getAssetProjects(data) {
      if( data.status == 'ok' ){
          assetProjects = data.data;
      }
  }
  
  var assetProjectRounds = {};
  function getAssetProjectRounds(data) {
      if( data.status == 'ok' ){
          assetProjectRounds = data.data;
      }
  }
  
  function buildSideBar() {
      // Attach asset rounds to their respective projects
      for(var i = 0; i < assetProjectRounds.length; i++) {
          for(var j = 0; j < assetProjects.length; j++) {
              if(assetProjectRounds[i]['project_id'] == assetProjects[j]['id']) {
                  assetProjects[j]['rounds'] = assetProjects[j]['rounds'] || [];
                  assetProjects[j]['rounds'].push(assetProjectRounds[i]);
                  break;
              }
          }
      }
      
      // Group Asset Projects by Category
      assetProjects = groupBy(assetProjects, 'category');

      if (isAdministrator) {
        const selectedCategories = `
            <div id="select-panel">
                <a id="selected-checkbox" class="label label-pill label-default" title="approved"><i class="fa fa-square-o" aria-hidden="true"></i></a>
                <button id="selected-action-button" type="button" class="btn btn-xs btn-warning disabled">Approve Selected</button>
            </div>
        `;
        $('#menu').append(selectedCategories);

        $('#selected-action-button').click(() => {
          if (onReview ? selectedOnReview.size : selectedOnApproved.size)
            $.ajax({
              url: `/admin/${onReview ? 'approve' : 'review'}-unit`,
              data: {
                projectIds: Array
                  .from(onReview ? selectedOnReview : selectedOnApproved)
                  .join(';')
              },
              method: 'GET',
              dataType: 'json',
              success: () => window.location.reload(),
              error: () => window.location.reload(),
            });
        })

        $('#selected-checkbox').click(() => {
          const selectedSetSize = onReview ? selectedOnReview.size : selectedOnApproved.size;

          $(`.select-toggle-${onReview ? 'review' : 'approved'}`)
            .each((toggle) =>
              updateTargetSelector($(`.select-toggle-${onReview ? 'review' : 'approved'}`)[toggle], !!selectedSetSize)
            );

          updateTopSelector();
        })
      }

      for(var i = 0; i < assetProjects.length; i++) {
          var category = assetProjects[i]['type'];
          
          // Build Current Category's Container
          var categoryContainer = '<div data-category-id="' + category + '" class="panel-group navbar-default category noRounds">' +
            '<div class="panel-heading" role="tab">' +
              '<h4>' + category + '<i class="pull-right fa fa-caret-down" aria-hidden="true"></i>' +
              '</h4>' +
            '</div>' +
            '<div class="round" data-value="' + category + '"></div>' +
            '</div>';
          $('#menu').append(categoryContainer);
          
          for(var j = 0; j < assetProjects[i]['data'].length; j++) {
              // Add Current Category's Projects
              var project = assetProjects[i]['data'][j];
              var status = project['approval'] == "Approved" ? 0 : 1;
              var nid = vid = project['id'];
              var dataCount = project['rounds'] ? project['rounds'].length : 0;
              var roundId = project['rounds'] && project['rounds'].length > 0 ? project['rounds'][0]['id'] : null;
              var highestRound = project['rounds'] && project['rounds'].length > 0 ? project['rounds'][0]['round'] : 0;

              var projectPassword = project['project_password'];
              var hasProjectPassword = projectPassword != '';
              var campaignPassword = $('.campaign-password').attr('title');
              var hasCampaignPassword = campaignPassword != '';
              var finalPassword;
              var passwordLabelClass = 'label-success'; // default if no passwords set
              if (hasProjectPassword) {
                  passwordLabelClass = 'label-danger';
                  finalPassword = projectPassword;
              } else if (hasCampaignPassword) {
                  passwordLabelClass = 'label-warning';
                  finalPassword = campaignPassword;
              }
              var passwordHintIcon = (hasProjectPassword || hasCampaignPassword) ? 'fa-lock' : 'fa-unlock';
              var passwordHint = '<span class="label label-pill ' + passwordLabelClass + '" title="' + finalPassword + '" ><i class="fa ' + passwordHintIcon + '" aria-hidden="true"></i></span> ';

              var linkHTML = "<a project-id='" + nid + "'" + (roundId != null ? " round-id='" + roundId + "'": "") + " highest-round='" + highestRound + "' href='#' class='round-link'>" + project['title'] + "</a>";
              var dropdownContainer = '' +
                '<div data-vid="' +vid+ '" data-status="' + status + '" class="panel panel-dropdown" role="tabpanel" aria-labelledby="" aria-expanded="false">' +
                  '<div class="row-content">' +
                    '<div class="list-group">' +
                      '<div class="list-group-item first-item">' +
                        (isAdministrator 
                          ? (
                            '<a id="select-toggle" project-id="' + nid + '" selected="0" class="select-toggle-' + (status ? 'review' : 'approved') + ' select-toggle-' + nid + ' label label-pill label-default mr-1" title="selector">' +
                              '<i class="fa fa-square-o" aria-hidden="true"></i>' +
                            '</a>'
                          )
                          : '') +
                        '<span class="dropdown-link glyphicon glyphicon-triangle-right" aria-hidden="true"></span>' +
                                          passwordHint +
                        linkHTML +
                        '<span class="badge">Round: ' + highestRound + '</span>' +
                      '</div>' +
                    '</div>' +
                    '<div id="' + nid + '" class="panel panel-dropdown-container rounds"></div>' +
                  '</div>' +
                '</div>';

              $('[data-value="' + category + '"]').append(dropdownContainer);
              if(project['rounds']) {
                  for(var k = 0; k < project['rounds'].length; k++) {
                    var round			= dataCount - k;

                    if (k === 0) { continue; }	// don't show the current round

                    var link = '' +
                      '<div data-sort="' + vid + '" class="list-group-item asset-round">' +
                        "<a project-id='" + nid + "'" + (roundId != null ? " round-id='" + project['rounds'][k]['id'] + "'": "") + " href='#' class='round-link'>" + project['title'] + "</a>" +
                        '<span class="badge">Round: ' + project['rounds'][k]['round'] /*round*/ + '</span>' +
                      '</div>';

                    $('#' + nid + '.rounds').append(link);
                  }
              }

              if (isAdministrator)
                $(`.select-toggle-${nid}`).click((e) => {
                  const wasSelected = !!Number(e.currentTarget.getAttribute('selected'));

                  updateTargetSelector(e.currentTarget, wasSelected);
                  updateTopSelector();
                })
          }

          $('[data-status="0"]').hide();
          $('#review').click(function (e) {
            e.preventDefault();
            onReview = true;
            if (isAdministrator) updateTopSelector();
            $('#approved').removeClass('active');
            $(this).addClass('active');
            $('[data-status="0"]').hide();
            $('[data-status="1"]').show();
          });
          $('#approved').click(function (e) {
            e.preventDefault();
            onReview = false;
            if (isAdministrator) updateTopSelector();
            $('#review').removeClass('active');
            $(this).addClass('active');
            $('[data-status="1"]').hide();
            $('[data-status="0"]').show();
          });
      }

      $('.panel-dropdown').each(function () {
          var dropdown = $(this);

          $("span.dropdown-link", dropdown).click(function (e) {
              e.preventDefault();
              dropdown.toggleClass('active');
              $('span.dropdown-link.glyphicon', dropdown).toggleClass('glyphicon-triangle-right').toggleClass('glyphicon-triangle-bottom');
              var div = $("div.panel-dropdown-container", dropdown);
              div.slideToggle('fast');
              $("div.panel-dropdown-container").not(div).slideUp('fast');
              $("span.dropdown-link").not(this).removeClass('glyphicon-triangle-bottom').removeClass('glyphicon-triangle-right').toggleClass('glyphicon-triangle-right');
              return false;
          });
      });

      $('.round-link').on('click', function(event){
          event.preventDefault();
          var id = $(this).attr('round-id');
          var pid = $(this).attr('project-id');

          // Hide the main campaign content info
          $('.campaign .content').hide();

          editProject.removeClass('display-none').attr('href', urls.editNode.format(pid, group_id, campaign_id, path.join('/')));

          var nextRoundEls = $('[data-vid='+pid+'] a[project-id='+pid+']');
          var nextRoundIndex = (nextRoundEls.length > 1) ? 1 : 0;
          var nextRound = nextRoundEls[nextRoundIndex].getAttribute('highest-round');

          nextRound = parseInt(nextRound) + 1;
          addAssetround.removeClass('display-none').attr('href', urls.addAssetround.format(group_id, pid, nextRound, campaign_id, path.join('/')));
          if(id) {
              editAssetround.removeClass('display-none').attr('href', urls.editNode.format(id, group_id, campaign_id, path.join('/')));
          } else {
              editAssetround.addClass('display-none');
          }
          buildContentDisplay(id);
          rebuildButtons();
      });
      
      // Updated REVIEW and APPROVED tabs with count for each
      let itemsReview = document.querySelectorAll('.select-toggle-review');
      let itemsApproved = document.querySelectorAll('.select-toggle-approved');
      let labelReview = document.querySelector('#review a');
      let labelApproved = document.querySelector('#approved a');
      if (labelReview !== null) {
        labelReview.innerHTML += ` (${itemsReview.length})`;
      }
      if (labelApproved !== null) {
        labelApproved.innerHTML += ` (${itemsApproved.length})`;
      }

  }

  function buildContentDisplay(id) {
      callAPI(endPoints.assetContent.format(id), {}, 'GET', 'JSON', true, function(data) {
          if(data.status == 'ok') {
              data = data.data[0];
              var content = $('#ajax-data');
              content.html("");
              if(data) {
                  content.append("<div class='project-title'>"+ data.title +  "</div><a href='{0}' target='_blank' class='btn btn-default project-btn'>Direct Link</a>".format(data.path) + data.assets);
                  content.show();
                  updateImageScales();
                  removeCommaSeparators();
                  buildVideoUploadPlayers();
              } else {
                  content.append("<b>NO ASSET ROUND TO DISPLAY. ADD ASSET ROUNDS ABOVE.</b>");
                  content.show();
              }
          }
      });
  }

  // Addition for Client Review Direct Link
  // Direct Links will have all the images on the page when document is ready
  $(document).ready(() => {
    updateImageScales();
    removeCommaSeparators();
  });

  function removeCommaSeparators() {
    document.querySelector('#ajax-data').childNodes.forEach(child => {
      if (child.nodeType === Node.TEXT_NODE && child.nodeValue.trim() === ',') 
        child.nodeValue = '' 
    })
  }

  function updateImageScales() {
    const imageAssets = $('.paragraph--type--image-asset');

    imageAssets.each(imageAsset => {
      const imageScaleComponent = $('.field--name-field-image-scale', imageAssets[imageAsset])[0];
      
      if (!imageScaleComponent) return;

      const imageScale = Number($('.field--item', imageScaleComponent)[0].getAttribute('content'));
      const images = $('.field--name-field-asset img', imageAssets[imageAsset]);

      imageScaleComponent.innerHTML = '';
      images.each(image => {
        images[image].style.width = `calc(var(--image-width) * ${imageScale / 100}px)`;
      })
    });
  }

  function buildVideoUploadPlayers() {

      // find all video upload assets
      jQuery('.paragraph--type--video-upload-asset').each(function (e) {

          function getFilename(path) {
              return path.replace(/\..+$/, '');
          }

          // get parent and sub-items (vids and captions)
          var vidAsset = jQuery(this);
          var vids = jQuery('.field--name-field-video-upload-asset .field--item .file-link a', vidAsset);
          var captions = jQuery('.field--name-field-video-captions-vtt-file', vidAsset);

          // get all caption files
          var captionFiles = {};
          captions.each(function (idx) {

              var captionEl = jQuery(this);
              var captionFile = jQuery('.field--item .file-link a', captionEl);
              var href = captionFile.attr('href');
              var fileName = getFilename(captionFile.text());
              captionFiles[fileName] = href;

              // remove the original item
              captionEl.remove();

          });

          // get all videos and create their tags
          vids.each(function (idx) {

              var vid = jQuery(this);
              var vidHref = vid.attr('href');
              var vidFilename = getFilename(vid.text());

              // create the video tag
              var vidtag = '<video controls="controls" width="640" height="480" class="video-upload-player">';
              vidtag += '<source src="' + vidHref + '" type="video/mp4">';

              // console.log('captionFiles:', captionFiles);
              // console.log('vidFilename:', vidFilename);

              // add subtitles if VTT caption file exists with the same name
              if (captionFiles.hasOwnProperty(vidFilename)) {
                  // console.log('FOUND caption:', captionFiles[vidFilename]);
                  vidtag += '<track label="English" kind="subtitles" srclang="en" src="' + captionFiles[vidFilename] + '" default="default">';
              }

              vidtag += '</video>';

              // make the tag into a jQuery element
              var vidPlay = jQuery(vidtag);

              // remove the original item, add the video tag in its place
              vid.closest('.field--item').empty().append(vidPlay);

              // load the video
              vidPlay.load();

          })

      });

  }
  
  // Create String format method prototype if it doesn't exist
  if (!String.prototype.format) {
      String.prototype.format = function() {
          var args = arguments;
          return this.replace(/{(\d+)}/g, function(match, number) { 
              return typeof args[number] != 'undefined'
                  ? args[number]
                  : match
                  ;
          });
      };
  }

function updateTargetSelector(target, wasSelected) {
  if (!isAdministrator) return;

  const projectId = target.getAttribute('project-id');

  target.setAttribute('selected', wasSelected ? '0' : '1');

  // Upading selected elements array
  if (wasSelected) {
    if (onReview) selectedOnReview.delete(projectId);
    else selectedOnApproved.delete(projectId);
  } else {
    if (onReview) selectedOnReview.add(projectId);
    else selectedOnApproved.add(projectId);
  }

  // Changing color
  target.classList.remove(wasSelected ? 'label-success' : 'label-default');
  target.classList.add(wasSelected ? 'label-default' : 'label-success');

  // Changing icon
  target.firstChild.classList.remove(wasSelected ? 'fa-check-square' : 'fa-square-o');
  target.firstChild.classList.add(wasSelected ? 'fa-square-o' : 'fa-check-square');
}

function updateTopSelector() {
  if (!isAdministrator) return;

  const checkbox = $('#selected-checkbox')[0];
  const checkboxIcon = $('i', checkbox)[0];
  const actionButton = $('#selected-action-button')[0];
  
  const states = {
    EMPTY: 'EMPTY',
    SOME: 'SOME',
    ALL: 'ALL',
  }

  const state = onReview
    ? selectedOnReview.size
      ? selectedOnReview.size === $('.select-toggle-review').length
        ? states.ALL
        : states.SOME
      : states.EMPTY
    : selectedOnApproved.size
      ? selectedOnApproved.size === $('.select-toggle-approved').length
        ? states.ALL
        : states.SOME
      : states.EMPTY

  // Resetting checkbox color styling
  checkbox.classList.remove('label-default', 'label-warning', 'label-success');

  // Changing color
  if (state === states.EMPTY) checkbox.classList.add('label-default');
  if (state === states.SOME) checkbox.classList.add('label-warning');
  if (state === states.ALL) checkbox.classList.add('label-success');


  // Resetting icon 
  checkboxIcon.classList.remove('fa-square-o', 'fa-minus-square', 'fa-check-square');

  // Changing icon
  if (state === states.EMPTY) checkboxIcon.classList.add('fa-square-o');
  if (state === states.SOME) checkboxIcon.classList.add('fa-minus-square');
  if (state === states.ALL) checkboxIcon.classList.add('fa-check-square');

  
  // Configuring if the button is disabled
  if (state === states.EMPTY) actionButton.classList.add('disabled');
  else actionButton.classList.remove('disabled');
  
  // Configuring the text inside the button
  if (onReview) {
    actionButton.innerHTML = 'Approve Selected'; // Updating wording in the button
    actionButton.classList.replace('btn-warning', 'btn-success'); // Replacing class for styling
  } else {
    actionButton.innerHTML = 'Move to Review'; // Updating wording in the button
    actionButton.classList.replace('btn-success', 'btn-warning'); // Replacing class for styling
  }
}

/**
 * takes array of objects and returns an object array grouped by provided key
 * @arg  {array}  arr  the array of objects
 * @arg  {string}  key  the key to sort by
 * @return  {object}  the sorted array object
 */
function groupBy(arr, key) {
  var newArr = [],
    types = {},
    i, j, cur;
  for (i = 0, j = arr.length; i < j; i++) {
    cur = arr[i];
    if (!(cur[key] in types)) {
      types[cur[key]] = {type: cur[key], data: []};
      newArr.push(types[cur[key]]);
    }
    types[cur[key]].data.push(cur);
  }
  return newArr;
}
  
  // TODO: This is a hack to get this working. We should figure out how to do this using hook_form_alter in php.
  function loginError() {
      $('.alert').each(function(){
          var alert = $(this)[0];
          if(alert.innerHTML.includes("password")) {
              alert.innerHTML = "Unrecognized username or password. Please try again or contact your dedicated producer.";
          }
      });
  }
  
})(window.jQuery, window.Drupal, window.Drupal.bootstrap);
