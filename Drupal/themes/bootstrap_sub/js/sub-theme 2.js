(function ($, Drupal, Bootstrap) {

	var path				= window.location.pathname.split('/'),
		root				= window.location.origin,
        currentClient       = path[2],
		endPoints			= {
			clientList      : root	+ '/api/clients',
            projectsList    : root  + '/api/projects/{0}/{1}',
            assetsList      : root  + '/api/assets/{0}/{1}',
            assetContent    : root  + '/api/content/{0}'
		},
        urls                = {
            addAssetround    : '/group/{0}/content/create/group_node:asset_round?edit[field_project]={1}&group={0}&campaign={2}&destination={3}',
            editNode         : '/node/{0}/edit?group={1}&campaign={2}&destination={3}'
        };
        
    var group = $('#group_id'),
        group_id = group[0] ? group[0].innerText : null,
        campaign = $('#campaign_id'),
        campaign_id = campaign[0] ? campaign[0].innerText : null;
    
    $(function(){
        // TODO: Should only be called on pages that need client list
        callAPI(endPoints.clientList, {}, 'GET', 'JSON', true, buildClientFilter);
        
        // TODO: Should only be called on pages that need project asset side bar
        callAPI(endPoints.projectsList.format(group_id, campaign_id), {}, 'GET', 'JSON', false, getAssetProjects);
        callAPI(endPoints.assetsList.format(group_id, campaign_id), {}, 'GET', 'JSON', false, getAssetProjectRounds);
        buildSideBar();
    });
    
	/**
	 * centralized API call function
	 * @arg  {String}    url       URL of endpoint to hit
	 * @arg  {Object}    data      data to pass to api if any
	 * @arg  {String}    method    call method GET|POST etc.
	 * @arg	 {string}    type      data type
	 * @arg  {Function}  callback  Success function
	 */	
	function callAPI(url,data,method,type,asyncronous,callback) {
		var self = this;
		$.ajax({
			url: url,
			data: data,
			method: method,
			dataType: type,
            async: asyncronous,
			success:function(data) {
				var response = {
					status	: 'ok',
					data	: data
				};
    
				callback(response);
			},
			error:function(jqXHR, textStatus, errorThrown){
				var response = {
					status : 'error',
					errors : [{
						code : jqXHR.status,
						message : errorThrown || textStatus
					}],
					data : null
				};
				
				callback(response);
			}
		});
	}
    
	/**
	 * receives JSON data from API and builds client list for filtering
	 * @arg  {object}  data  ajax response from /api/clients/
	 */
	function buildClientFilter(data) {
        var clientDropdown = $('#client-dropdown');
		if(data.status === 'ok' && clientDropdown) {
			data = data.data;
    
			$('#client-dropdown').append(
				'<button class="btn btn-default project-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
				'<span class="caret"></span>' +
				'Client Filter' +
				'</button>' +
				'<ul class="client-dropdown dropdown-menu" aria-labelledby="dropdownMenu1"></ul>'
            );
			$('#client-dropdown').show();
			$('#client-dropdown ul').append('<li><a href="/">All</a></li>');
            var selectedClientValue = "Client Filter";
            var selectedClientName = "Client Filter"
			$(data).each(function (i,v) {
				var client = v.label;
                var clientUri = client.replace(' ', '-');
                selectedClientValue = (currentClient == clientUri) ? clientUri : selectedClientValue;
                selectedClientName = (currentClient == clientUri) ? client : selectedClientName;
				$('#client-dropdown ul').append('<li><a href="/clients/' + clientUri + '" data-value="' + clientUri + '">' + client + '</a></li>');
			});
            $('#client-dropdown .btn').val(selectedClientValue);
			$('#client-dropdown .btn').html('<span class="caret"></span> ' + selectedClientName);
		}
	}
    
    var assetProjects = {};
    function getAssetProjects(data) {
        if( data.status == 'ok' ){
            assetProjects = data.data;
        }
    }
    
    var assetProjectRounds = {};
    function getAssetProjectRounds(data) {
        if( data.status == 'ok' ){
            assetProjectRounds = data.data;
        }
    }
    
    
    // TODO: This needs to be completely redone. All data is coming back properly.
    function buildSideBar() {
        for(var i = 0; i < assetProjectRounds.length; i++) {
            for(var j = 0; j < assetProjects.length; j++) {
                if(assetProjectRounds[i]['project_id'] == assetProjects[j]['id']) {
                    assetProjects[j]['rounds'] = assetProjects[j]['rounds'] || [];
                    assetProjects[j]['rounds'].push(assetProjectRounds[i]);
                    break;
                }
            }
        }
        assetProjects = groupBy(assetProjects, 'category');
        // for(var i = 0; i < assetProjects.length; i++) {
        //     assetProjects[i]['data'] = groupBy(assetProjects[i]['data'], 'approval');
        // }
        
        for(var i = 0; i < assetProjects.length; i++) {
            var cat = assetProjects[i]['type'];
            
    		var categoryContainer = '<div data-category-id=' + cat + ' class="panel-group navbar-default category noRounds">' +
    			'<div class="panel-heading" role="tab">' +
    			'<h4>' + cat + '<i class="pull-right fa fa-caret-down" aria-hidden="true"></i>' +
    			'</h4>' +
    			'</div>' +
    			'<div class="round" data-value=' + cat + '></div>' +
    			'</div>';
            $('#menu').append(categoryContainer);
            for(var j = 0; j < assetProjects[i]['data'].length; j++) {
                var project = assetProjects[i]['data'][j];
                var status = project['approval'];
                var nid = vid = project['id'];
                var dataCount = project['rounds'] ? project['rounds'].length : 0;
                var roundId = project['rounds'] && project['rounds'].length > 0 ? project['rounds'][0]['id'] : null;
                var linkHTML = "<a project-id='" + nid + "'" + (roundId != null ? " round-id='" + roundId + "'": "") + " href='#' class='round-link'>" + project['title'] + "</a>";
				var dropdownContainer = '' +
					'<div data-vid="' +vid+ '" data-status="' + status + '" class="panel panel-dropdown" role="tabpanel" aria-labelledby="" aria-expanded="false">' +
						'<div class="row-content">' +
							'<div class="list-group">' +
								'<div class="list-group-item first-item">' +
									'<span class="dropdown-link glyphicon glyphicon-triangle-right" aria-hidden="true"></span>' +
									linkHTML +
									'<span class="badge">Round: ' + dataCount + '</span>' +
								'</div>' +
							'</div>' +
							'<div id="' + nid + '" class="panel panel-dropdown-container rounds"></div>' +
						'</div>' +
					'</div>';

				$('[data-value="' + cat + '"]').append(dropdownContainer);
				$('[data-category-id="' + cat + '"]').toggleClass('noRounds');
            }
        }
        
        $('.round-link').on('click', function(event){
            event.preventDefault();
            var id = $(this).attr('round-id');
            var pid = $(this).attr('project-id');
            var editProject = $('#edit-project-btn');
            var addAssetround = $('#add-asset-round-btn');
            var editAssetround = $('#edit-asset-round-btn');
            
            editProject.removeClass('display-none').attr('href', urls.editNode.format(pid, group_id, campaign_id, path.join('/')));
            addAssetround.removeClass('display-none').attr('href', urls.addAssetround.format(group_id, pid, campaign_id, path.join('/')));
            if(id) {
                editAssetround.removeClass('display-none').attr('href', urls.editNode.format(id, group_id, campaign_id, path.join('/')));
            } else {
                editAssetround.addClass('display-none');
            }
            buildContentDisplay(id);
            
            for(var i = 0; i < Drupal.ajax.instances.length; i++) {
                var instance = Drupal.ajax.instances[i];
                switch(instance.selector) {
                    case "#edit-project-btn":
                        instance.url = editProject.attr('href');
                        instance.element_settings.url = instance.url;
                        instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                        break;
                    case "#add-asset-round-btn":
                        instance.url = addAssetround.attr('href');
                        instance.element_settings.url = instance.url;
                        instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                        break;
                    case "#edit-asset-round-btn":
                        instance.url = editAssetround.attr('href');
                        instance.element_settings.url = instance.url;
                        instance.options.url = instance.url + "&_wrapper_format=drupal_modal";
                        break;
                }
            }
        });
    }
    
    function buildContentDisplay(id) {
        callAPI(endPoints.assetContent.format(id), {}, 'GET', 'JSON', true, function(data) {
            if(data.status == 'ok') {
                data = data.data[0];
                var content = $('#ajax-data');
                content.html("");
                if(data) {
                    content.append(data.title + "<a href='{0}' target='_blank' class='btn btn-default project-btn'>Direct Link</a>".format(data.path) + data.assets);
                    content.show();
                } else {
                    content.append("<b>NO ASSET ROUND TO DISPLAY. ADD ASSET ROUNDS ABOVE.</b>");
                    content.show();
                }
            }
        });
    }
    
    // Create String format method prototype if it doesn't exist
    if (!String.prototype.format) {
        String.prototype.format = function() {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function(match, number) { 
                return typeof args[number] != 'undefined'
                    ? args[number]
                    : match
                    ;
            });
        };
    }
    
	/**
	 * takes array of objects and returns an object array grouped by provided key
	 * @arg  {array}  arr  the array of objects
	 * @arg  {string}  key  the key to sort by
	 * @return  {object}  the sorted array object
	 */
	function groupBy(arr, key) {
		var newArr = [],
			types = {},
			i, j, cur;
		for (i = 0, j = arr.length; i < j; i++) {
			cur = arr[i];
			if (!(cur[key] in types)) {
				types[cur[key]] = {type: cur[key], data: []};
				newArr.push(types[cur[key]]);
			}
			types[cur[key]].data.push(cur);
		}
		return newArr;
	}
    
    /*** THIS IS OLD ORIGINAL CODE ***/
    /*** USEFUL AS REFERENCE ***/
    
	// var path				= window.location.pathname.split('/'),
	// 	root				= window.location.origin,
	// 	topLevelPaths		= ['','tvgla','user','tvgla-editor','asset-display'],
	// 	clientFilteredPaths	= ['','tvgla-editor'],
	// 	isTopLevel			= topLevelPaths.indexOf(path[1]) > -1,
	// 	isClientFiltered	= clientFilteredPaths.indexOf(path[1]) > -1,
	// 	projectId			= path[3] || path[2],
	// 	assetID				= path[4] || null,
	// 	revisionID			= path[5] || null,
	// 	endPoints			= {
	// 		clientList      : root	+ '/api/clients'
	// 		projectList     : root	+ '/api/project/nav/'	+ projectId,
	// 		projectAssets   : root	+ '/api/project/'		+ projectId
	// 	},
	// 	badge				= 'Rounds: ' + 0,
	// 	changed				= '',
	// 	categories			= {},
	// 	assets				= [];
    // 
	// $(function(){
	// 	// Top Level Paths
	// 	if (isTopLevel) {
    // 
	// 		$('.sidebar__toggle').hide();
	// 		
	// 		// Client Filterable Paths
	// 		if(isClientFiltered) {
    // 
	// 			// Logout Button
	// 			$('section#block-front-editor-tools').append('<a href="/user/logout" class="btn btn-default project-btn logout">Logout</a>');
    // 
	// 			// GET Client Filter List
	// 			callAPI(endPoints.clientList,{},'GET','JSON',buildClientFilter);
	// 		}
	// 	}
    // 
	// 	// Project/Asset Paths
	// 	else {
	// 		
	// 		/**
	// 		 * Sidebar Toggle
	// 		 */
	// 		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	// 			$('.sidebar__toggle').show();
	// 			$('[data-toggle=offcanvas]').click(function () {
	// 				$('#main').toggle();
	// 				$('#sidebar').toggle().toggleClass('mobile');
	// 				$('.sidebar__toggle').toggleClass('active');
	// 			});
	// 		}
    // 
	// 		// AJAX Preloader
	// 		$.ajaxSetup({
	// 			beforeSend: function () {
	// 				$(".uil-ring-css").show();
	// 			},
	// 			complete: function () {
	// 				$(".uil-ring-css").hide();
	// 				$('#main .region-content .views-field-field-image a.colorbox').addClass('cboxElement');
	// 			}
	// 		});
    // 
	// 		// Get Project Categories
	// 		callAPI(endPoints.projectList,{},'GET','JSON',buildCategories);
    // 
	// 		// Get Project Assets
	// 		callAPI(endPoints.projectAssets,{},'GET','JSON',buildAssets);
	// 	}
    // 
	// });
    // 
	// /**
	//  * receives JSON data from API and builds project categories list for client
	//  * @arg  {Object}  data  ajax response from /api/project/nav/
	//  */
	// function buildCategories(data) {
	// 	if(data.status === 'ok') {
	// 		data = data.data;
    // 
	// 		$(data).each(function () {
	// 			categories[this.nid] = this;
	// 		});
    // 
	// 		$(data).each(function () {
	// 			var categoryContainer = '<div data-category-id=' + this.nid + ' class="panel-group navbar-default category noRounds">' +
	// 				'<div class="panel-heading" role="tab">' +
	// 				'<h4>' + this.title + '<i class="pull-right fa fa-caret-down" aria-hidden="true"></i>' +
	// 				'</h4>' +
	// 				'</div>' +
	// 				'<div class="round" data-value=' + this.nid + '></div>' +
	// 				'</div>';
    // 
	// 			$('#menu').append(categoryContainer);
	// 		});
    // 
	// 		$('#block-projectmenublock').show();
	// 	}
	// }
    // 
	// /**
	//  * receives JSON data from API and builds assets and project sidebar
	//  * @arg  {Object}  data  the Ajax response from api/project
	//  * @return  {[type]}  [description]
	//  */
	// function buildAssets(data) {
	// 	if(data.status === 'ok') {
	// 		data = data.data;
    // 
	// 		// Loop through returned data and construct assets array
	// 		$(data).each(function(i,v){
	// 			parseAsset(v);
	// 		});
	// 		
	// 		// sort assets by nid
	// 		var sorted_assets = groupBy(assets, 'nid');
	// 		
	// 		// loop through sorted assets and build sidebar links and asset html
	// 		$(sorted_assets).each(function () {
	// 			var proj		= this.data,
	// 				round1		= proj.length - 1,
	// 				nid			= proj[round1].nid,
	// 				vid			= proj[0].vid,
	// 				categoryId	= proj[round1].cid,
	// 				status		= proj[0].status,
	// 				dataCount	= proj.length,
	// 				linkHTML	= buildLink(proj[0],dataCount);
    // 
	// 			if (dataCount > 0) {
	// 				var dropdownContainer = '' +
	// 					'<div data-vid="' +vid+ '" data-status="' + status + '" class="panel panel-dropdown" role="tabpanel" aria-labelledby="" aria-expanded="false">' +
	// 						'<div class="row-content">' +
	// 							'<div class="list-group">' +
	// 								'<div class="list-group-item first-item">' +
	// 									'<span class="dropdown-link glyphicon glyphicon-triangle-right" aria-hidden="true"></span>' +
	// 									linkHTML +
	// 									'<span class="badge">Round: ' + dataCount + '</span>' +
	// 								'</div>' +
	// 							'</div>' +
	// 							'<div id="' + nid + '" class="panel panel-dropdown-container rounds"></div>' +
	// 						'</div>' +
	// 					'</div>';
    // 
	// 				$('[data-value="' + categoryId + '"]').append(dropdownContainer);
	// 				$('[data-category-id="' + categoryId + '"]').toggleClass('noRounds');
	// 				buildAsset(proj[0],dataCount);
	// 			}
    // 
	// 			// Children Rounds
	// 			if (dataCount > 1) {
	// 				
	// 				$(proj).each(function (i) {
	// 					
	// 					var round			= dataCount - i
	// 						roundLinkHtml	= buildLink(this,round);
	// 					
	// 					if (i === 0) { return; }	// don't show the current round
	// 					else { buildAsset(this,round); }	// build the asset html
    // 
	// 					var link = '' +
	// 						'<div data-sort="' + this.vid + '" class="list-group-item asset-round">' +
	// 							roundLinkHtml +
	// 							'<span class="badge">Round: ' + round + '</span>' +
	// 						'</div>';
    // 
	// 					$('#' + this.nid + '.rounds').append(link);
	// 					
	// 				});
	// 			}
    // 
	// 			// Review / Approved Tabs
	// 			$('[data-status="0"]').hide();
	// 			$('#review').click(function (e) {
	// 				e.preventDefault();
	// 				$('#approved').removeClass('active');
	// 				$(this).addClass('active');
	// 				$('[data-status="0"]').hide();
	// 				$('[data-status="1"]').show();
	// 			});
	// 			$('#approved').click(function (e) {
	// 				e.preventDefault();
	// 				$('#review').removeClass('active');
	// 				$(this).addClass('active');
	// 				$('[data-status="1"]').hide();
	// 				$('[data-status="0"]').show();
	// 			});
	// 		});
    // 
	// 		// Hide empty categories for clients
	// 		if ($('#block-projectmenublock').length == 0) {
	// 			$('.noRounds').hide();
	// 		}
    // 
	// 		// Rounds DropDown
	// 		$('.panel-dropdown').each(function () {
	// 			var dropdown = $(this);
    // 
	// 			$("span.dropdown-link", dropdown).click(function (e) {
	// 				e.preventDefault();
	// 				dropdown.toggleClass('active');
	// 				$('span.dropdown-link.glyphicon', dropdown).toggleClass('glyphicon-triangle-right').toggleClass('glyphicon-triangle-bottom');
	// 				var div = $("div.panel-dropdown-container", dropdown);
	// 				div.slideToggle('fast');
	// 				$("div.panel-dropdown-container").not(div).slideUp('fast');
	// 				return false;
	// 			});
	// 		});
    // 
	// 		// Link content mapping
	// 		$('.panel-dropdown .list-group-item a.internal-link').click(function (e) {
	// 			e.preventDefault();
    // 
	// 			var link				= $(this),
	// 				vid					= link.attr('data-vid'),
	// 				nid					= link.attr('data-nid'),
	// 				round				= $('#ajax-data [data-vid="' + vid + '"]'),
	// 				tools				= round.children('.tools').html(),
	// 				titleHTML			= round.children('.title').html(),
	// 				image_containerHTML	= round.children('.image').html(),
	// 				video_containerHTML	= round.children('.video').html(),
	// 				container			= $('#main .region-content');
    // 
	// 			if(container.find('.contextual-region').length < 1) {
	// 				container.html('<div class="contextual-region">'
	// 					+ '<div class="view-content">'
	// 						+ '<div class="views-view-grid horizontal cols-1 clearfix">'
	// 							+ '<div class="list-item">'
	// 								+ '<div class="views-col col-1" style="width: 100%;">'
	// 									+ '<div class="views-field views-field-title project-display__title"><span class="field-content"></span></div>'
	// 									+ '<div class="views-field views-field-field-image"><div class="field-content"></div></div>'
	// 								+ '</div>'
	// 							+ '</div>'
	// 						+ '</div>'
	// 					+ '</div>'
	// 				);
	// 			}
    // 
	// 			var title_container		= $('#main .region-content .views-field-title .field-content'),
	// 				image_container		= $('#main .region-content .views-field-field-image .field-content');
    // 
	// 			$('.tabs').replaceWith(tools);
	// 			title_container.replaceWith('<div class="field-content">' + titleHTML + '</div>');
    // 
	// 			if (video_containerHTML) {
	// 				image_container.replaceWith('<div class="field-content">' + video_containerHTML + '</div>');
	// 			} else {
	// 				image_container.replaceWith('<div class="field-content">' + image_containerHTML + '</div>');
	// 			}
    // 
	// 			if ($('.col-1 .deadline').length === 0) {
	// 				$('.col-1').append($('#ajax-data [data-vid="' + vid + '"] .deadline').html());
	// 			} else if ($('.col-1 .revision-log').length === 0) {
	// 				$('.col-1').append($('#ajax-data [data-vid="' + vid + '"] .revision-log').html());
	// 			} else {
	// 				$('.col-1 .deadline').replaceWith($('#ajax-data [data-vid="' + vid + '"] .deadline').html());
	// 				$('.col-1 .revision-log').replaceWith($('#ajax-data [data-vid="' + vid + '"] .revision-log').html());
	// 			}
    // 
	// 			if ($('.editor-tools').length > 0) {
	// 				$('.editor-tools').remove();
	// 				$('#block-projectmenublock').append('<a href="/node/' + nid +'/edit" class="use-ajax btn btn-default project-btn editor-tools" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;80%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Edit</a>' +
	// 					'<a href="/node/' + nid +'/edit?revision=true" class="use-ajax btn btn-default project-btn editor-tools" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;80%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Add Revision</a>');
	// 				$('#block-projectmenublock').append('<a href="/node/' + nid +'/delete" class="use-ajax btn btn-default project-btn editor-tools" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;80%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Delete</a>');
	// 			} else {
	// 				$('#block-projectmenublock').append('<a href="/node/' + nid +'/edit" class="use-ajax btn btn-default project-btn editor-tools" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;80%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Edit</a>' +
	// 					'<a href="/node/' + nid +'/edit?revision=true" class="use-ajax btn btn-default project-btn editor-tools" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;80%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Add Revision</a>');
	// 				$('#block-projectmenublock').append('<a href="/node/' + nid +'/delete" class="use-ajax btn btn-default project-btn editor-tools" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;80%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Delete</a>');
	// 			}
    // 
	// 			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	// 				$('#sidebar').toggle();
	// 				$('.sidebar__toggle').toggleClass('active');
	// 				$('#main').toggle();
	// 			}
    // 
	// 		});
    // 
	// 		$('.add-asset').click(function(){
	// 			$(document).ajaxComplete(function(){
	// 				$('#drupal-modal .js-form-file').last().click(function(){
	// 					$('#drupal-modal .field--type-image').hide();
	// 				});
    // 
	// 				$('#drupal-modal .js-form-file').first().click(function(){
	// 					$('#drupal-modal .field--type-video').hide();
	// 				});
	// 			});
	// 		});
    // 
	// 		if(assetID) {
	// 			if(revisionID) {
	// 				$('.panel-dropdown').find('[data-nid='+assetID+'][data-vid='+revisionID+']').click();
	// 			}
	// 			else {
	// 				$('.panel-dropdown').find('[data-nid='+assetID+']:first').click();
	// 			}
	// 		}
    // 
	// 		Drupal.attachBehaviors();
	// 	}
    // 
	// 	// ERROR
	// 	else {
	// 		window.location.replace("/");
	// 	}
    // 
	// 	/**
	// 	 * generate sidebar link
	// 	 * @arg  {object}  asset  the asset
	// 	 * @return  {string}  HTML string
	// 	 */
	// 	function buildLink(asset,round) {
	// 		var linkHTML = '';
	// 		
	// 		if (asset.externalUrl !== '') {
	// 			linkHTML = '<a data-nid="' + asset.nid + '" data-vid="' + asset.vid + '" target="_blank" href="' + asset.externalUrl + '" hreflang="en">' + asset.title + '</a>';
	// 		} else if (asset.iFrameUrl !== '') {
	// 			linkHTML = '<a data-nid="' + asset.nid + '" data-vid="' + asset.vid + '" class="ajax cboxElement" href="' + asset.iFrameUrl + '" hreflang="en">' + asset.title + '</a>';
	// 		} else {
	// 			linkHTML = '<a data-nid="' + asset.nid + '" data-vid="' + asset.vid + '" class="internal-link" href="' + asset.directLink + '/round-' + round + '" hreflang="en">' + asset.title + '</a>';
	// 		}
    // 
	// 		return linkHTML;
	// 	}
    // 
	// 	/**
	// 	 * parses asset data into asset array
	// 	 * @arg  {object}  asset  the asset to parse
	// 	 */
	// 	function parseAsset(asset){
	// 		changed = asset.changed[0].value || '';
    // 
	// 		var contentType = asset.type[0].target_id;
    // 
	// 		if (contentType === 'tvgla_asset' || contentType === 'tvgla_html') {
    // 
	// 			if (asset.field_category.length > 0) {
    // 
	// 				var image		= {},
	// 					video		= {},
	// 					imageGroup	= [],
	// 					videoGroup	= [],
	// 					revisionLog	= '',
	// 					deadline	= '',
	// 					externalUrl	= '',
	// 					iFrameUrl	= '';
    // 
	// 				// Image Assets
	// 				if (asset.field_image.length === 1) {
    // 
	// 					image = buildImg(asset.field_image[0]);
    // 
	// 				} else if (asset.field_image.length > 1) {
    // 
	// 					$(asset.field_image).each(function (){
	// 						imageGroup.push(buildImg(this));
	// 					});
	// 				}
    // 
	// 				// Video Assets
	// 				if (asset.field_video.length === 1) {
	// 					video = buildVid(asset.field_video[0]);
    // 
	// 				} else if (asset.field_video.length > 1) {
    // 
	// 					$(asset.field_video).each(function (){
	// 						videoGroup.push(buildVid(this));
	// 					});
	// 				}
    // 
	// 				if (asset.field_external_link_url.length > 0) {
	// 					externalUrl = asset.field_external_link_url[0].value;
	// 				}
	// 				if (asset.field_iframe_url.length > 0) {
	// 					iFrameUrl = asset.field_iframe_url[0].value;
	// 				}
	// 				if (asset.field_deadline.length > 0) {
	// 					date = new Date(asset.field_deadline[0].value);
	// 					deadline = 'Deadline: ' +(date.getMonth()+1) + '-' + date.getDate() + '-' + date.getFullYear();
	// 				}
	// 				if (asset.revision_log.length > 0) {
	// 					revisionLog = 'Revision Log: ' + asset.revision_log[0].value;
	// 				}
    // 
	// 				var object = {
	// 					nid			: asset.nid[0].value,
	// 					vid			: asset.vid[0].value,
	// 					cid			: asset.field_category[0].target_id,
	// 					status		: asset.field_review_status[0].value,
	// 					title		: asset.title[0].value,
	// 					created		: asset.created[0].value,
	// 					updated		: changed,
	// 					image		: image,
	// 					video		: video,
	// 					imageGroup	: imageGroup,
	// 					videoGroup	: videoGroup,
	// 					externalUrl	: externalUrl,
	// 					iFrameUrl	: iFrameUrl,
	// 					deadline	: deadline,
	// 					revisionLog	: revisionLog,
	// 					projectURL	: asset.field_project[0].url
	// 				};
	// 				object.directLink = buildDirectUrl(object);
    // 
	// 				assets.push(object);
	// 			}
	// 		}
	// 	}
    // 
	// 	/**
	// 	 * parses video data
	// 	 * @arg  {object}  vid  the video object to parse
	// 	 * @return  {object}  the reconstructed video object
	// 	 */
	// 	function buildVid(vid) {
	// 		return {
	// 			alt		: vid.description,
	// 			url		: vid.url,
	// 			type	: vid.url.split('.').pop()
	// 		};
	// 	}
	// 	/**
	// 	 * parses image data
	// 	 * @arg  {object}  img  the image object to parse
	// 	 * @return  {object}  the reconstructed image object
	// 	 */
	// 	function buildImg(img) {
	// 		return {
	// 			title	: img.title,
	// 			alt		: img.alt,
	// 			url		: img.url,
	// 			height	: img.height,
	// 			width	: img.width
	// 		}
	// 	}
    // 
	// 	/**
	// 	 * builds direct asset url
	// 	 * @arg  {object}  asset  the assets to link to
	// 	 * @return  {string}  the direct asset URL
	// 	 */
	// 	function buildDirectUrl(asset) {
	// 		function cleanURL(string) {
	// 			return string.replace(/[^\w]+/g,'-').replace(/-$/,'').toLowerCase();
	// 		}
	// 		return asset.projectURL + '/' + asset.nid + '/' + asset.vid + '/' + cleanURL(categories[asset.cid].title) + '/' + cleanURL(asset.title);
	// 	}
    // 
	// 	/**
	// 	 * builds video asset container for one or more videos
	// 	 * @arg  {object}  asset                   the asset
	// 	 * @arg  {string}  deadline_container      html string for dieadline
	// 	 * @arg  {string}  revision_log_container  html string for revision log
	// 	 * @arg  {string}  direct_asset_link       html string with direct asset link
	// 	 * @return  {array}  array of html strings for video assets
	// 	 */
	// 	function buildVidContainer(asset,deadline_container,revision_log_container,direct_asset_link) {
	// 		var alt_container = '',
	// 			video_container = [];
    // 
	// 		if (asset.videoGroup.length > 1) {
	// 			$(asset.videoGroup).each(function (i,v) {
	// 				var video = v,
	// 					html = '';
	// 				
	// 				alt_container = asset.video.alt ? '<div class="caption"><span class="field-content alt">' + asset.video.alt + '</span></div>' : '';
    // 
	// 				if(i === 0) {
	// 					html += direct_asset_link;
	// 				}
	// 				html += '<a href="' + asset.video.url + '">' +
	// 						'<video class="video-js vjs-default-skin" controls preload="metadata" poster="/themes/bootstrap_sub/images/large-logo.png" style="width: auto; height: auto; max-width: 600px; background-color: #FFFFFF; border: 1px solid #ddd; margin-bottom: 50px; min-height: 300px;"><source src="'+ asset.video.url +'" type="video/' + asset.video.type + '" /></video></a>' +
	// 						alt_container +
	// 						deadline_container +
	// 						revision_log_container;
	// 				
	// 				video_container.push(html);
	// 			});
	// 		} else if (asset.video.url && asset.videoGroup.length === 0) {
	// 			
	// 			alt_container = asset.video.alt ? '<div class="caption"><span class="field-content alt">' + asset.video.alt + '</span></div>' : '';
    // 
	// 			video_container.push(direct_asset_link + 
	// 				'<a href="' + asset.video.url + '">' +
	// 				'<video class="video-js vjs-default-skin" controls preload="metadata" poster="/themes/bootstrap_sub/images/large-logo.png" style="width: auto; height: auto; min-height: 300px; max-width: 600px; background-color: #FFFFFF; border: 1px solid #ddd; margin-bottom: 50px;"><source src="'+ asset.video.url +'" type="video/' + asset.video.type + '" /></video></a>' +
	// 				alt_container +
	// 				deadline_container +
	// 				revision_log_container);
	// 		}
    // 
	// 		return video_container;
	// 	}
    // 
	// 	/**
	// 	 * builds image asset container for one or more images
	// 	 * @arg  {object}  asset                   the asset
	// 	 * @arg  {string}  deadline_container      html string for dieadline
	// 	 * @arg  {string}  revision_log_container  html string for revision log
	// 	 * @arg  {string}  direct_asset_link       html string with direct asset link
	// 	 * @return  {array}  array of html strings for image assets
	// 	 */
	// 	function buildImgContainer(asset,deadline_container,revision_log_container,direct_asset_link) {
	// 		var alt_container = '',
	// 			image_container = [];
    // 
	// 		if (asset.imageGroup.length > 1) {
	// 			$(asset.imageGroup).each(function (i,v) {
	// 				var img = v,
	// 					html = '';
	// 				
	// 				alt_container	= img.alt ? '<div class="caption"><span class="field-content alt">' + img.alt + '</span></div>' : '';
    // 
	// 				if(i === 0) {
	// 					html += direct_asset_link;
	// 				}
	// 				html += '<div class="imageGroup">' +
	// 							'<a href="' + img.url + '" class="colorbox cboxElement" title="' + img.title + '" >' +
	// 								'<img height="' + img.height + '" width="' + img.width + '" src="' + img.url + '" alt="' + img.alt + '" typeof="foaf:Image" class="ajax-image img-responsive" />' +
	// 							'</a>' +
	// 							alt_container +
	// 							deadline_container +
	// 							revision_log_container +
	// 						'</div>'
    // 
	// 				image_container.push(html);
	// 			});
	// 		}
	// 		else if (asset.image.url && asset.imageGroup.length === 0){
	// 			alt_container	= asset.image.alt ? '<div class="caption"><span class="field-content alt">' + asset.image.alt + '</span></div>' : '';
    // 
	// 			image_container.push(direct_asset_link + 
	// 				'<a href="' + asset.image.url + '" class="colorbox cboxElement" title="' + asset.image.title + '" >' +
	// 				'<img height="' + asset.image.height + '" width="' + asset.image.width + 'px" src="' + asset.image.url + '" alt="' + asset.image.alt + '" typeof="foaf:Image" class="ajax-image img-responsive"></a>' +
	// 				alt_container +
	// 				deadline_container +
	// 				revision_log_container);
	// 		}
    // 
	// 		return image_container;
	// 	}
    // 
	// 	/**
	// 	 * build the navigation html string for an asset
	// 	 * @arg  {object}  asset  the asset
	// 	 * @return  {string}  html string
	// 	 */
	// 	function buildNav(asset){
	// 		return '' +
	// 			'<div class="tools">' +
	// 				'<nav class="tabs" role="sidebar_navigation" aria-label="Tabs">' +
	// 					'<ul class="tabs--primary nav nav-tabs">' +
	// 						'<li class="active">' +
	// 							'<a href="/node/' + asset.nid + '/edit" class="use-ajax" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;75%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Edit</a>' +
	// 						'</li>' +
	// 						'<li class="active">' +
	// 							'<a href="/node/' + asset.nid + '/edit?revision=true" class="use-ajax" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;75%&quot;,&quot;0&quot;:&quot;height&quot;,&quot;1&quot;:700}">Add Revision</a>' +
	// 						'</li>' +
	// 					'</ul>' +
	// 				'</nav>' +
	// 			'</div>';
	// 	}
    // 
	// 	/**
	// 	 * builds the html for an asset
	// 	 * @arg  {object}  asset  the asset
	// 	 */
	// 	function buildAsset(asset,round){
	// 		var deadline_container		= asset.deadline ? '<div class="caption"><span class="field-content deadline">' + asset.deadline + '</span></div>' : '',
	// 			revision_log_container	= asset.revisionLog ? '<div class="caption"><span class="field-content revision-log">' + asset.revisionLog + '</span></div>' : '',
	// 			direct_asset_link		= '<a class="btn btn-default project-btn" href="' + asset.directLink + '/round-'+ round +'" target="_blank">Direct Asset Link</a>',
    // 
	// 			video_container			= buildVidContainer(asset,deadline_container,revision_log_container,direct_asset_link),
	// 			image_container			= buildImgContainer(asset,deadline_container,revision_log_container,direct_asset_link),
    // 
	// 			navigationTemplate		= buildNav(asset),
    // 
	// 			html					= '' +
	// 									'<div data-vid="' + asset.vid + '">' +
	// 										navigationTemplate +
	// 										'<div class="title" data-cid="' + asset.cid + '">' +
	// 											'<span class="ajax-title field-content">' + asset.title + '</span>' +
	// 										'</div>' +
	// 										'<div class="image">' + image_container.join('') + '</div>' +
	// 										'<div class="video">' + video_container.join('') + '</div>' +
	// 									'</div>';
    // 
	// 		$('#ajax-data').append(html);
	// 	}
	// }
	
})(window.jQuery, window.Drupal, window.Drupal.bootstrap);
