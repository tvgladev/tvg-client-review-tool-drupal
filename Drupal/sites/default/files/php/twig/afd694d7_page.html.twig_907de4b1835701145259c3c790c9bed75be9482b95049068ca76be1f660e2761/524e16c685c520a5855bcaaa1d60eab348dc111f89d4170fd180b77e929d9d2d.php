<?php

/* themes/bootstrap_sub/templates/page.html.twig */
class __TwigTemplate_8b58634cdc05d91a0879de25882c55d11396e29f84cff93eec8b6284c902f88e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'main' => array($this, 'block_main'),
            'sidebar_first' => array($this, 'block_sidebar_first'),
            'navbar' => array($this, 'block_navbar'),
            'header' => array($this, 'block_header'),
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'action_links' => array($this, 'block_action_links'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 1, "if" => 3);
        $filters = array();
        $functions = array("path" => 3);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('block', 'if'),
                array(),
                array('path')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        $this->displayBlock('main', $context, $blocks);
    }

    public function block_main($context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"wrapper\">
    ";
        // line 3
        if ((((isset($context["is_front"]) ? $context["is_front"] : null) ||  !(isset($context["logged_in"]) ? $context["logged_in"] : null)) || twig_in_filter("tvgla-editor", $this->env->getExtension('drupal_core')->getPath("<current>")))) {
            // line 4
            echo "    <div class=\"frontpage__header\" id=\"header\">
        <div class=\"column col-sm-4 col-centered\">
            <div class=\"frontpage__logo\">
                <a href=\"";
            // line 7
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getPath("<front>")));
            echo "\">
                    <img class=\"frontpage__logo-img img-responsive\" src=\"/themes/bootstrap_sub/images/large-logo.png\">
                </a>
            </div>
        </div>
    </div>
    ";
        }
        // line 14
        echo "    <div class=\"row row-offcanvas row-offcanvas-left\">
        <div class=\"sidebar__toggle\" style=\"display: none\"><a href=\"#\" data-toggle=\"offcanvas\"><i class=\"fa fa-navicon fa-2x\"></i></a></div>
        <!-- sidebar -->
        ";
        // line 17
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())) {
            // line 18
            echo "        ";
            // line 19
            echo "        <div class=\"column col-sm-3 sidebar-offcanvas scrollable\" id=\"sidebar\">
                ";
            // line 20
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 23
            echo "            <div class='uil-ring-css' style='transform:scale(0.35);'><div></div></div>
            <div class=\"nav\" id=\"menu\">
                <ul class=\"tabs--primary nav nav-tabs\">
                    <li id=\"review\" class=\"active\"><a href=\"#\" data-drupal-link-system-path=\"#\">Review</a></li>
                    <li id=\"approved\" ><a href=\"#\" data-drupal-link-system-path=\"#\">Approved</a></li>
                </ul>
            </div>
        </div>
        <!-- /sidebar -->

        <!-- main right col -->
        <div class=\"column col-sm-9 projectpage_main-container\" id=\"main\">
        ";
        } else {
            // line 36
            echo "            <div class=\"column col-sm-12 frontpage__project-container\" id=\"main\">
        ";
        }
        // line 38
        echo "        ";
        // line 39
        echo "        ";
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array()))) {
            // line 40
            echo "            ";
            $this->displayBlock('navbar', $context, $blocks);
            // line 45
            echo "        ";
        }
        // line 46
        echo "
            ";
        // line 48
        echo "            ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array())) {
            // line 49
            echo "                ";
            $this->displayBlock('header', $context, $blocks);
            // line 54
            echo "            ";
        }
        // line 55
        echo "
            ";
        // line 57
        echo "            ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array())) {
            // line 58
            echo "                ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 61
            echo "            ";
        }
        // line 62
        echo "
            ";
        // line 64
        echo "            ";
        if ((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null)) {
            // line 65
            echo "                ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 68
            echo "            ";
        }
        // line 69
        echo "
            ";
        // line 71
        echo "            ";
        if ((isset($context["action_links"]) ? $context["action_links"] : null)) {
            // line 72
            echo "                ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 75
            echo "            ";
        }
        // line 76
        echo "
            ";
        // line 78
        echo "            ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array())) {
            // line 79
            echo "                ";
            $this->displayBlock('help', $context, $blocks);
            // line 82
            echo "            ";
        }
        // line 83
        echo "
            ";
        // line 85
        echo "            ";
        $this->displayBlock('content', $context, $blocks);
        // line 98
        echo "            <div id=\"ajax-data\" style=\"display: none\"></div>
        </div>
        <!-- /main -->
    </div>
</div>
";
    }

    // line 20
    public function block_sidebar_first($context, array $blocks = array())
    {
        // line 21
        echo "                    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()), "html", null, true));
        echo "
                ";
    }

    // line 40
    public function block_navbar($context, array $blocks = array())
    {
        // line 41
        echo "                <nav class=\"navbar navbar-static-top\">
                    ";
        // line 42
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()), "html", null, true));
        echo "
                </nav>
            ";
    }

    // line 49
    public function block_header($context, array $blocks = array())
    {
        // line 50
        echo "                    <div role=\"heading\">
                        ";
        // line 51
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
                    </div>
                ";
    }

    // line 58
    public function block_highlighted($context, array $blocks = array())
    {
        // line 59
        echo "                    <div class=\"highlighted\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
        echo "</div>
                ";
    }

    // line 65
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 66
        echo "                    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "html", null, true));
        echo "
                ";
    }

    // line 72
    public function block_action_links($context, array $blocks = array())
    {
        // line 73
        echo "                    <ul class=\"action-links\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["action_links"]) ? $context["action_links"] : null), "html", null, true));
        echo "</ul>
                ";
    }

    // line 79
    public function block_help($context, array $blocks = array())
    {
        // line 80
        echo "                    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array()), "html", null, true));
        echo "
                ";
    }

    // line 85
    public function block_content($context, array $blocks = array())
    {
        // line 86
        echo "                ";
        if ( !(isset($context["logged_in"]) ? $context["logged_in"] : null)) {
            // line 87
            echo "                <div class=\"column col-sm-3 col-centered\">
                    <a id=\"main-content\"></a>
                    ";
            // line 89
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
            echo "
                </div>
                ";
        } elseif (        // line 91
(isset($context["logged_in"]) ? $context["logged_in"] : null)) {
            // line 92
            echo "                    ";
            if ( !(isset($context["is_admin"]) ? $context["is_admin"] : null)) {
                // line 93
                echo "                    ";
            }
            // line 94
            echo "                    <a id=\"main-content\"></a>
                    ";
            // line 95
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
            echo "
                ";
        }
        // line 97
        echo "            ";
    }

    public function getTemplateName()
    {
        return "themes/bootstrap_sub/templates/page.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  304 => 97,  299 => 95,  296 => 94,  293 => 93,  290 => 92,  288 => 91,  283 => 89,  279 => 87,  276 => 86,  273 => 85,  266 => 80,  263 => 79,  256 => 73,  253 => 72,  246 => 66,  243 => 65,  236 => 59,  233 => 58,  226 => 51,  223 => 50,  220 => 49,  213 => 42,  210 => 41,  207 => 40,  200 => 21,  197 => 20,  188 => 98,  185 => 85,  182 => 83,  179 => 82,  176 => 79,  173 => 78,  170 => 76,  167 => 75,  164 => 72,  161 => 71,  158 => 69,  155 => 68,  152 => 65,  149 => 64,  146 => 62,  143 => 61,  140 => 58,  137 => 57,  134 => 55,  131 => 54,  128 => 49,  125 => 48,  122 => 46,  119 => 45,  116 => 40,  113 => 39,  111 => 38,  107 => 36,  92 => 23,  90 => 20,  87 => 19,  85 => 18,  83 => 17,  78 => 14,  68 => 7,  63 => 4,  61 => 3,  58 => 2,  52 => 1,);
    }
}
/* {% block main %}*/
/* <div class="wrapper">*/
/*     {% if is_front or not logged_in or 'tvgla-editor' in path('<current>') %}*/
/*     <div class="frontpage__header" id="header">*/
/*         <div class="column col-sm-4 col-centered">*/
/*             <div class="frontpage__logo">*/
/*                 <a href="{{ path('<front>') }}">*/
/*                     <img class="frontpage__logo-img img-responsive" src="/themes/bootstrap_sub/images/large-logo.png">*/
/*                 </a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="row row-offcanvas row-offcanvas-left">*/
/*         <div class="sidebar__toggle" style="display: none"><a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a></div>*/
/*         <!-- sidebar -->*/
/*         {% if page.sidebar_first %}*/
/*         {# Nav Toggle #}*/
/*         <div class="column col-sm-3 sidebar-offcanvas scrollable" id="sidebar">*/
/*                 {% block sidebar_first %}*/
/*                     {{ page.sidebar_first }}*/
/*                 {% endblock %}*/
/*             <div class='uil-ring-css' style='transform:scale(0.35);'><div></div></div>*/
/*             <div class="nav" id="menu">*/
/*                 <ul class="tabs--primary nav nav-tabs">*/
/*                     <li id="review" class="active"><a href="#" data-drupal-link-system-path="#">Review</a></li>*/
/*                     <li id="approved" ><a href="#" data-drupal-link-system-path="#">Approved</a></li>*/
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /sidebar -->*/
/* */
/*         <!-- main right col -->*/
/*         <div class="column col-sm-9 projectpage_main-container" id="main">*/
/*         {% else %}*/
/*             <div class="column col-sm-12 frontpage__project-container" id="main">*/
/*         {% endif %}*/
/*         {# Navbar #}*/
/*         {% if page.navigation or page.navigation_collapsible %}*/
/*             {% block navbar %}*/
/*                 <nav class="navbar navbar-static-top">*/
/*                     {{ page.navigation }}*/
/*                 </nav>*/
/*             {% endblock %}*/
/*         {% endif %}*/
/* */
/*             {# Header #}*/
/*             {% if page.header %}*/
/*                 {% block header %}*/
/*                     <div role="heading">*/
/*                         {{ page.header }}*/
/*                     </div>*/
/*                 {% endblock %}*/
/*             {% endif %}*/
/* */
/*             {# Highlighted #}*/
/*             {% if page.highlighted %}*/
/*                 {% block highlighted %}*/
/*                     <div class="highlighted">{{ page.highlighted }}</div>*/
/*                 {% endblock %}*/
/*             {% endif %}*/
/* */
/*             {# Breadcrumbs #}*/
/*             {% if breadcrumb %}*/
/*                 {% block breadcrumb %}*/
/*                     {{ breadcrumb }}*/
/*                 {% endblock %}*/
/*             {% endif %}*/
/* */
/*             {# Action Links #}*/
/*             {% if action_links %}*/
/*                 {% block action_links %}*/
/*                     <ul class="action-links">{{ action_links }}</ul>*/
/*                 {% endblock %}*/
/*             {% endif %}*/
/* */
/*             {# Help #}*/
/*             {% if page.help %}*/
/*                 {% block help %}*/
/*                     {{ page.help }}*/
/*                 {% endblock %}*/
/*             {% endif %}*/
/* */
/*             {# Content #}*/
/*             {% block content %}*/
/*                 {% if not logged_in %}*/
/*                 <div class="column col-sm-3 col-centered">*/
/*                     <a id="main-content"></a>*/
/*                     {{ page.content }}*/
/*                 </div>*/
/*                 {% elseif logged_in %}*/
/*                     {% if not is_admin %}*/
/*                     {% endif %}*/
/*                     <a id="main-content"></a>*/
/*                     {{ page.content }}*/
/*                 {% endif %}*/
/*             {% endblock %}*/
/*             <div id="ajax-data" style="display: none"></div>*/
/*         </div>*/
/*         <!-- /main -->*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
