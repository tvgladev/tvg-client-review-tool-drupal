<?php

/* core/modules/system/templates/entity-add-list.html.twig */
class __TwigTemplate_d00e1f993f21e969a81707bd65cd4a9ae9db193c912fde0ec4b36d87822cbc7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 19, "for" => 21);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if', 'for'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 19
        if ( !twig_test_empty((isset($context["bundles"]) ? $context["bundles"] : null))) {
            // line 20
            echo "  <dl>
    ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["bundles"]) ? $context["bundles"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["bundle"]) {
                // line 22
                echo "      <dt>";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["bundle"], "add_link", array()), "html", null, true));
                echo "</dt>
      <dd>";
                // line 23
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["bundle"], "description", array()), "html", null, true));
                echo "</dd>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bundle'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "  </dl>
";
        } elseif ( !twig_test_empty(        // line 26
(isset($context["add_bundle_message"]) ? $context["add_bundle_message"] : null))) {
            // line 27
            echo "  <p>
    ";
            // line 28
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["add_bundle_message"]) ? $context["add_bundle_message"] : null), "html", null, true));
            echo "
  </p>
";
        }
    }

    public function getTemplateName()
    {
        return "core/modules/system/templates/entity-add-list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 28,  70 => 27,  68 => 26,  65 => 25,  57 => 23,  52 => 22,  48 => 21,  45 => 20,  43 => 19,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to present a list of available bundles.*/
/*  **/
/*  * Available variables:*/
/*  *   - bundles: A list of bundles, each with the following properties:*/
/*  *     - label: Bundle label.*/
/*  *     - description: Bundle description.*/
/*  *     - add_link: Link to create an entity of this bundle.*/
/*  *   - add_bundle_message: The message shown when there are no bundles. Only*/
/*  *                         available if the entity type uses bundle entities.*/
/*  **/
/*  * @see template_preprocess_entity_add_list()*/
/*  **/
/*  * @ingroup themeable*/
/*  *//* */
/* #}*/
/* {% if bundles is not empty %}*/
/*   <dl>*/
/*     {% for bundle in bundles %}*/
/*       <dt>{{ bundle.add_link }}</dt>*/
/*       <dd>{{ bundle.description }}</dd>*/
/*     {% endfor %}*/
/*   </dl>*/
/* {% elseif add_bundle_message is not empty %}*/
/*   <p>*/
/*     {{ add_bundle_message }}*/
/*   </p>*/
/* {% endif %}*/
/* */
