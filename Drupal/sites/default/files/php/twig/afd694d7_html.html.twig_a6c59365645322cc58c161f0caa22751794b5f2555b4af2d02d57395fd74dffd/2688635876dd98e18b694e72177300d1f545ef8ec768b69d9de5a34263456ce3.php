<?php

/* themes/bootstrap_sub/templates/html.html.twig */
class __TwigTemplate_e1fbb64dd62639e209db62ef677f599f362e09425dbea8ad91a3764463d3363a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 47);
        $filters = array("clean_class" => 49, "raw" => 58, "safe_join" => 59, "t" => 69);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set'),
                array('clean_class', 'raw', 'safe_join', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 47
        $context["body_classes"] = array(0 => ((        // line 48
(isset($context["logged_in"]) ? $context["logged_in"] : null)) ? ("user-logged-in") : ("")), 1 => (( !        // line 49
(isset($context["root_path"]) ? $context["root_path"] : null)) ? ("path-frontpage") : (("path-" . \Drupal\Component\Utility\Html::getClass((isset($context["root_path"]) ? $context["root_path"] : null))))), 2 => ((        // line 50
(isset($context["node_type"]) ? $context["node_type"] : null)) ? (("page-node-type-" . \Drupal\Component\Utility\Html::getClass((isset($context["node_type"]) ? $context["node_type"] : null)))) : ("")), 3 => ((        // line 51
(isset($context["db_offline"]) ? $context["db_offline"] : null)) ? ("db-offline") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 52
(isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_position", array())) ? (("navbar-is-" . $this->getAttribute($this->getAttribute((isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_position", array()))) : ("")), 5 => (($this->getAttribute(        // line 53
(isset($context["theme"]) ? $context["theme"] : null), "has_glyphicons", array())) ? ("has-glyphicons") : ("")));
        // line 55
        echo "<!DOCTYPE html>
<html ";
        // line 56
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["html_attributes"]) ? $context["html_attributes"] : null), "html", null, true));
        echo " ng-app=\"reviewTool\">
<head>
    <head-placeholder token=\"";
        // line 58
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\">
        <title>";
        // line 59
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->safeJoin($this->env, (isset($context["head_title"]) ? $context["head_title"] : null), " | ")));
        echo "</title>
        <css-placeholder token=\"";
        // line 60
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\"></css-placeholder>
        <js-placeholder token=\"";
        // line 61
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\"></js-placeholder>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,500' rel='stylesheet' type='text/css'>
        <link href=\"http://vjs.zencdn.net/5.11.6/video-js.css\" rel=\"stylesheet\">
        <!-- If you'd like to support IE8 -->
        <script src=\"http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js\"></script>
</head>
    <body";
        // line 67
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["body_classes"]) ? $context["body_classes"] : null)), "method"), "html", null, true));
        echo ">
        <a href=\"#main-content\" class=\"visually-hidden focusable skip-link\">
            ";
        // line 69
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Skip to main content")));
        echo "
        </a>
        ";
        // line 71
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["page_top"]) ? $context["page_top"] : null), "html", null, true));
        echo "
        ";
        // line 72
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["page"]) ? $context["page"] : null), "html", null, true));
        echo "
        ";
        // line 73
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["page_bottom"]) ? $context["page_bottom"] : null), "html", null, true));
        echo "
        <js-bottom-placeholder token=\"";
        // line 74
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["placeholder_token"]) ? $context["placeholder_token"] : null)));
        echo "\"></js-bottom-placeholder>
        <script src=\"https://use.fontawesome.com/a9dc1bbbfc.js\"></script>
        <script src=\"http://vjs.zencdn.net/5.11.6/video.js\"></script>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "themes/bootstrap_sub/templates/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 74,  98 => 73,  94 => 72,  90 => 71,  85 => 69,  80 => 67,  71 => 61,  67 => 60,  63 => 59,  59 => 58,  54 => 56,  51 => 55,  49 => 53,  48 => 52,  47 => 51,  46 => 50,  45 => 49,  44 => 48,  43 => 47,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to display the basic html structure of a single*/
/*  * Drupal page.*/
/*  **/
/*  * Variables:*/
/*  * - $css: An array of CSS files for the current page.*/
/*  * - $language: (object) The language the site is being displayed in.*/
/*  *   $language->language contains its textual representation.*/
/*  *   $language->dir contains the language direction. It will either be 'ltr' or*/
/*  *   'rtl'.*/
/*  * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.*/
/*  * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.*/
/*  * - $head_title: A modified version of the page title, for use in the TITLE*/
/*  *   tag.*/
/*  * - $head_title_array: (array) An associative array containing the string parts*/
/*  *   that were used to generate the $head_title variable, already prepared to be*/
/*  *   output as TITLE tag. The key/value pairs may contain one or more of the*/
/*  *   following, depending on conditions:*/
/*  *   - title: The title of the current page, if any.*/
/*  *   - name: The name of the site.*/
/*  *   - slogan: The slogan of the site, if any, and if there is no title.*/
/*  * - $head: Markup for the HEAD section (including meta tags, keyword tags, and*/
/*  *   so on).*/
/*  * - $styles: Style tags necessary to import all CSS files for the page.*/
/*  * - $scripts: Script tags necessary to load the JavaScript files and settings*/
/*  *   for the page.*/
/*  * - $page_top: Initial markup from any modules that have altered the*/
/*  *   page. This variable should always be output first, before all other dynamic*/
/*  *   content.*/
/*  * - $page: The rendered page content.*/
/*  * - $page_bottom: Final closing markup from any modules that have altered the*/
/*  *   page. This variable should always be output last, after all other dynamic*/
/*  *   content.*/
/*  * - $classes String of classes that can be used to style contextually through*/
/*  *   CSS.*/
/*  **/
/*  * @see bootstrap_preprocess_html()*/
/*  * @see template_preprocess()*/
/*  * @see template_preprocess_html()*/
/*  * @see template_process()*/
/*  **/
/*  * @ingroup templates*/
/*  *//* */
/* #}*/
/* {% set body_classes = [*/
/* logged_in ? 'user-logged-in',*/
/* not root_path ? 'path-frontpage' : 'path-' ~ root_path|clean_class,*/
/* node_type ? 'page-node-type-' ~ node_type|clean_class,*/
/* db_offline ? 'db-offline',*/
/* theme.settings.navbar_position ? 'navbar-is-' ~ theme.settings.navbar_position,*/
/* theme.has_glyphicons ? 'has-glyphicons',*/
/* ] %}*/
/* <!DOCTYPE html>*/
/* <html {{ html_attributes }} ng-app="reviewTool">*/
/* <head>*/
/*     <head-placeholder token="{{ placeholder_token|raw }}">*/
/*         <title>{{ head_title|safe_join(' | ') }}</title>*/
/*         <css-placeholder token="{{ placeholder_token|raw }}"></css-placeholder>*/
/*         <js-placeholder token="{{ placeholder_token|raw }}"></js-placeholder>*/
/*         <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,500' rel='stylesheet' type='text/css'>*/
/*         <link href="http://vjs.zencdn.net/5.11.6/video-js.css" rel="stylesheet">*/
/*         <!-- If you'd like to support IE8 -->*/
/*         <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>*/
/* </head>*/
/*     <body{{ attributes.addClass(body_classes) }}>*/
/*         <a href="#main-content" class="visually-hidden focusable skip-link">*/
/*             {{ 'Skip to main content'|t }}*/
/*         </a>*/
/*         {{ page_top }}*/
/*         {{ page }}*/
/*         {{ page_bottom }}*/
/*         <js-bottom-placeholder token="{{ placeholder_token|raw }}"></js-bottom-placeholder>*/
/*         <script src="https://use.fontawesome.com/a9dc1bbbfc.js"></script>*/
/*         <script src="http://vjs.zencdn.net/5.11.6/video.js"></script>*/
/*     </body>*/
/* </html>*/
