<?php

/* themes/bootstrap_sub/templates/block--system-branding-block.html.twig */
class __TwigTemplate_02a778715c4f151d904b9a575b541300f4685c6d85f793261c990996fa2233e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("block--bare.html.twig", "themes/bootstrap_sub/templates/block--system-branding-block.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "block--bare.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 19);
        $filters = array("t" => 29);
        $functions = array("path" => 21);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array('t'),
                array('path')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
        // line 19
        echo "    ";
        if ((isset($context["is_front"]) ? $context["is_front"] : null)) {
            // line 20
            echo "        <div class=\"frontpage__branding\">
            <div class=\"frontpage__branding-logo\"><a class=\"logo-link\" href=\"";
            // line 21
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getPath("<front>")));
            echo "\"><img src=\"../images/large-logo.png\" height=\"140\" width=\"449\"></a> </div>
            <div class=\"frontpage__branding-title\">
                ";
            // line 23
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
            echo "
            </div>
        </div>
    ";
        } else {
            // line 27
            echo "        ";
            if ((isset($context["site_logo"]) ? $context["site_logo"] : null)) {
                // line 28
                echo "            <a href=\"/user/logout\" class=\"btn btn-default project-btn logout\">Logout</a>
            <a class=\"logo navbar-btn pull-right\" href=\"";
                // line 29
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getPath("<front>")));
                echo "\" title=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Home")));
                echo "\" rel=\"home\">
                <img src=\"";
                // line 30
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["site_logo"]) ? $context["site_logo"] : null), "html", null, true));
                echo "\" alt=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Home")));
                echo "\" />
            </a>
            <a class=\"toggle\" href=\"#\" data-toggle=\"offcanvas\"><i class=\"fa fa-navicon fa-2x\"></i></a>
        ";
            }
            // line 34
            echo "        ";
            if ((isset($context["site_name"]) ? $context["site_name"] : null)) {
                // line 35
                echo "            <a class=\"name navbar-brand\" href=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getPath("<front>")));
                echo "\" title=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Home")));
                echo "\" rel=\"home\">";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["site_name"]) ? $context["site_name"] : null), "html", null, true));
                echo "</a>
        ";
            }
            // line 37
            echo "        ";
            if ((isset($context["site_slogan"]) ? $context["site_slogan"] : null)) {
                // line 38
                echo "            <p class=\"navbar-text\">";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["site_slogan"]) ? $context["site_slogan"] : null), "html", null, true));
                echo "</p>
        ";
            }
            // line 40
            echo "    ";
        }
    }

    public function getTemplateName()
    {
        return "themes/bootstrap_sub/templates/block--system-branding-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 40,  110 => 38,  107 => 37,  97 => 35,  94 => 34,  85 => 30,  79 => 29,  76 => 28,  73 => 27,  66 => 23,  61 => 21,  58 => 20,  55 => 19,  52 => 18,  11 => 1,);
    }
}
/* {% extends "block--bare.html.twig" %}*/
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation for a branding block.*/
/*  **/
/*  * Each branding element variable (logo, name, slogan) is only available if*/
/*  * enabled in the block configuration.*/
/*  **/
/*  * Available variables:*/
/*  * - site_logo: Logo for site as defined in Appearance or theme settings.*/
/*  * - site_name: Name for site as defined in Site information settings.*/
/*  * - site_slogan: Slogan for site as defined in Site information settings.*/
/*  **/
/*  * @ingroup templates*/
/*  *//* */
/* #}*/
/* {% block content %}*/
/*     {% if is_front %}*/
/*         <div class="frontpage__branding">*/
/*             <div class="frontpage__branding-logo"><a class="logo-link" href="{{ path('<front>') }}"><img src="../images/large-logo.png" height="140" width="449"></a> </div>*/
/*             <div class="frontpage__branding-title">*/
/*                 {{ title }}*/
/*             </div>*/
/*         </div>*/
/*     {% else %}*/
/*         {% if site_logo %}*/
/*             <a href="/user/logout" class="btn btn-default project-btn logout">Logout</a>*/
/*             <a class="logo navbar-btn pull-right" href="{{ path('<front>') }}" title="{{ 'Home'|t }}" rel="home">*/
/*                 <img src="{{ site_logo }}" alt="{{ 'Home'|t }}" />*/
/*             </a>*/
/*             <a class="toggle" href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a>*/
/*         {% endif %}*/
/*         {% if site_name %}*/
/*             <a class="name navbar-brand" href="{{ path('<front>') }}" title="{{ 'Home'|t }}" rel="home">{{ site_name }}</a>*/
/*         {% endif %}*/
/*         {% if site_slogan %}*/
/*             <p class="navbar-text">{{ site_slogan }}</p>*/
/*         {% endif %}*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
