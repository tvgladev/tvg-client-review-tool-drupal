function usage() {
	echo
	echo "Usage: ./check-cert.sh <domain>"
	echo
	echo "Example:"
	echo
	echo "  ./check-cert.sh review.tvgla.com"
	echo
}

if [ $# -eq 0 ]; then
	usage
	exit 1
fi

if [ -z "$1" ]; then
	usage
	exit 1
fi


DOMAIN="$1"
CHECK_PAUSE=5

while :
	do
		# nslookup -type=text "_acme-challenge.${DOMAIN}"
		dig TXT "_acme-challenge.${DOMAIN}" | grep _acme-challenge
		sleep ${CHECK_PAUSE}
		echo "Press [CTL+C] to stop ..."
	done