function usage() {
	echo
	echo "Usage: ./create-cert.sh <domain>"
	echo
	echo "Example:"
	echo
	echo "  ./create-cert.sh review.tvgla.com"
	echo
}

if [ $# -eq 0 ]; then
	usage
	exit 1
fi

if [ -z "$1" ]; then
	usage
	exit 1
fi


DOMAIN="$1"

docker run -it --rm \
    -v "$(pwd)/letsencrypt:/etc/letsencrypt" \
    certbot/certbot certonly --manual \
    --preferred-challenges dns \
    --email larry@tvgla.com \
    --no-eff-email \
    -d $DOMAIN
