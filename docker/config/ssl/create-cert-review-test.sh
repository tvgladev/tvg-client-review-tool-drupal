docker run -it --rm \
    -v "$(pwd)/letsencrypt:/etc/letsencrypt" \
    certbot/certbot certonly --manual \
    --preferred-challenges dns \
    --email larry@tvgla.com \
    --no-eff-email \
    -d review-test.tvgla.com
