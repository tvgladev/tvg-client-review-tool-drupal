# INSTRUCTIONS

The latest production database needs to be in this folder in order for the MariaDB container build to succeed.

To pull the latest prod database, do the following:

* Open terminal at the root of this repository
* Run the following command to dump the prod database to the ECS host:
     * ```yarn ecs:crt:dbdump:prod2stghost```
     * You will need to enter the prod database password
* Run the following command to pull the prod database from the ECS host to local:
     * ```yarn ecs:crt:dbdump:pull2local```
     * The following file will be saved:
          * ```docker/config/mariadb/clientreview_prod_db.sql```