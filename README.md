# TVGla Client Review Tool

- [TVGla Client Review Tool](#tvgla-client-review-tool)
  - [Deployment and Hosting Diagram](#deployment-and-hosting-diagram)
  - [Initial Setup](#initial-setup)
    - [Set up SSH for ```tvgecsd```](#set-up-ssh-for-tvgecsd)
      - [Download the SSH key file](#download-the-ssh-key-file)
      - [Update your ~/.ssh/config file](#update-your-sshconfig-file)
      - [Test your SSH connections](#test-your-ssh-connections)
    - [Set up AWS credentials](#set-up-aws-credentials)
  - [Project Folder Structure](#project-folder-structure)
  - [Running Locally and Remotely](#running-locally-and-remotely)
  - [Key Paths in Docker container](#key-paths-in-docker-container)
  - [ENV Vars for Local Dockercompose \& AWS ECS Container](#env-vars-for-local-dockercompose--aws-ecs-container)
    - [Drupal Container Env Vars](#drupal-container-env-vars)
    - [MariaDB Container Env Vars (local only)](#mariadb-container-env-vars-local-only)
- [AWS Hosting Info](#aws-hosting-info)
  - [Mounting EFS file server on EC2 if needed](#mounting-efs-file-server-on-ec2-if-needed)
    - [Set up auto-mount on restart](#set-up-auto-mount-on-restart)
  - [Copying Uploaded Files from efs-backup to efs](#copying-uploaded-files-from-efs-backup-to-efs)
  - [Ensuring That File Uploads Can Work](#ensuring-that-file-uploads-can-work)


## Deployment and Hosting Diagram

![Deployment and Hosting Diagram](CRT-Deployment-Diagram.png)

## Initial Setup

### Set up SSH for ```tvgecsd```

You need SSH set up to run some of the npm scripts that ssh or scp with the EC2 instance used for hosting the CRT.

#### Download the SSH key file

* Log in to the TVGla VPN
* Go to afp://10.20.10.2/Visionaire/Resources/Dig_Dev/_SecureData/TVGla/AWS/SSH Keys
* Download the file ```aws-tvgla-dev.pem``` and place it inside your ```~/.ssh/``` folder

#### Update your ~/.ssh/config file

* Add the following lines to your ~/.ssh/config file

```
# AWS TVGla ECS "TVG-Dockers" instance
# CLient Review Tool - review.tvgla.com
Host tvgecsd
    # Hostname ec2-54-226-87-160.compute-1.amazonaws.com
    Hostname ec2-54-242-117-221.compute-1.amazonaws.com
    User ec2-user
    IdentityFile ~/.ssh/aws-tvgla-dev.pem

# AWS Client-Review-Tool-Prod EC2 instance for ECS
# Client Review Tool TEST - review-test.tvgla.com
Host crtecs
    Hostname ec2-18-206-208-114.compute-1.amazonaws.com
    User ec2-user
    IdentityFile ~/.ssh/aws-tvgla-dev.pem
```

#### Test your SSH connections

In terminal try the following:

* ```ssh tvgecsd```
* ```ssh crtecs```

Both should allow you to log in if everything is set up correctly.


### Set up AWS credentials

You need AWS configured to push and pull from the EC2 instance and other AWS services.

* Install aws-cli if it's not installed
* Get the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY values
* Run ```aws configure``` and enter the values when prompted

## Project Folder Structure

| Folder | Description |
|--------|-------------|
| docker | Scripts and files for building and managing Docker images |
| Drupal | The Client Review Tool's Drupal files |
| prepend | A file for making ENV vars available to $_VHOST |

## Running Locally and Remotely

| Description | Command |
| ----------- | --------|
|
**<u style="font-size: 24px">1. PULL PROD DB TO LOCAL, CREATE SSL CERTS</u>** | *********************** |
| 1) Pull Prod db to stage server<br/><i> &nbsp;&nbsp; - Required in order to build the MariaDB container</i> <br/><i> &nbsp;&nbsp; - You will be asked for the clientreview_prod_db password</i> | ```yarn ecs:crt:dbdump:prod2stghost``` |
| 2) Pull Prod db from stage to local<br/><i> &nbsp;&nbsp; - Required in order to build the MariaDB container</i> <br/><i> &nbsp;&nbsp; - Saves in docker/config/mariadb</i> | ```yarn ecs:crt:dbdump:pull2local``` |
| 3) Create SSL Cert Files (run before first build<br/><i> &nbsp;&nbsp; - Only needed if existing SSL Certs are expired</i><br/><i> &nbsp;&nbsp; - Requires access to tvgla.com DNS host</i> | ```yarn crt:ssl:create:local```<br />```yarn crt:ssl:check:local``` |
|
**<u style="font-size: 24px;">2. BUILD DOCKER IMAGES</u>** | *********************** |
| Build Drupal and Mariadb Images | ```yarn crt:build``` |
| 
**<u style="font-size: 24px">3. START APP IN DOCKER CONTAINERS</u>** | *********************** |
| Bring App up in Containers | ```yarn crt:up``` |
| Restart App after code updates | ```yarn crt:restart``` |
| Bring App down | ```yarn crt:down``` |
|
**<u style="font-size: 24px">4. VIEW APP IN LOCAL BROWSER</u>** | *********************** |
| Update local ```/etc/hosts``` file | Add line:<br /> ```127.0.0.1 review-local.tvgla.com``` |
| View app in local browser (non-SSL) -Should show remote images | ```http://review-local.tvgla.com:8088``` |
| View app in local browser (SSL) - May not display remote images | ```https://review-local.tvgla.com:8443``` |
**<u style="font-size: 24px">5. ACCESS LOCAL SHELL &amp; FIX PERMS</u>** | *********************** |
| Fix local file upload permissions | ```yarn crt:chmod``` |
| View Drupal container shell | ```yarn crt:sh``` |
| 
**<u style="font-size: 24px">6. TAG &amp; PUSH DOCKER IMAGE TO AWS ECR</u>** | *********************** |
| Run all of the below in one command | ```yarn ecr:img:all``` |
| Build AMD version for AWS<br />- The ARM version will not work on AWS servers | ```yarn crt:build:dru:amd``` |
| Login to AWS ECR | ```yarn ecr:login``` |
| Tag latest Drupal image | ```yarn ecr:img:tag``` |
| Push latest Drupal image | ```yarn ecr:img:push``` |
|
**<u style="font-size: 24px">7. UPDATE AWS ECS SERVICE WITH LATEST IMAGE</u>** | *********************** |
| Update ECS Service | ```yarn ecs:service:update``` |
| View ECS Service Status | ```yarn ecs:service:status``` | 
|
**<u style="font-size: 24px">8. UPDATE SSL CERT ON LIVE ECS SITE</u>** | *********************** |
| Create new review.tvgla.com SSL cert | ```yarn crt:ssl:create:prod```<br/>```yarn crt:ssl:check:prod``` |
| Push cert to ECS docker container and refresh Apache | ```yarn crt:ssl:push:prod``` |


## Key Paths in Docker container

| Path | Description |
| ---- | ----------- |
| Drupal root | ```/bitnami/drupal/``` |
| Drupal settings | ```/bitnami/drupal/sites/default/settings.php``` |
| Drupal uploads (in-container) | ```/bitnami/drupal/sites/default/files``` |
| Drupal uploads ECS EFS volume<br />The above path is mapped to this volume on the ECS instance | ```/efs/crt-uploaded-files``` |
| Drush phar | ```/bitnami/drupal/drush.phar``` |
| Apache config | ```/opt/bitnami/apache/conf/httpd.conf``` |
| Apache SSL config | ```/opt/bitnami/apache/conf/extra/httpd-ssl.conf``` |
| Apache SSL certs | ```/opt/bitnami/apache/conf/fullchain.pem```<br />```/opt/bitnami/apache/conf/privkey.pem``` |
| .bashrc | ```/root/.bashrc``` |
| App entrypoint | ```/app-entrypoint.sh``` |


## ENV Vars for Local Dockercompose & AWS ECS Container

### Drupal Container Env Vars

| ENV Var Name | Local Value | Remote Value |
| ------------ | ----------- | ------------ |
| ```DRUPAL_DATABASE_USER``` | root | root |
| ```DRUPAL_DATABASE_PASS``` | <i>[blank]</i> | <i>[not-shown-here]</i> |
| ```DRUPAL_DATABASE_HOST``` | mariadb | tvg-crt-autoscaling.cacpwuqovbo6.us-east-1.rds.amazonaws.com |
| ```DRUPAL_DATABASE_PORT``` | 3306 | 3306 |
| ```DRUPAL_DATABASE_NAME``` | clientreview_db | Stg: client_review_db, Prod: clientreview_prod_db |
| ```DRUPAL_USERNAME``` | user | <i>[not-shown-here]</i> |
| ```DRUPAL_PASSWORD``` | bitnami | <i>[not-shown-here]</i> |
| ```ALLOW_EMPTY_PASSWORD``` | yes | yes | assuming this is right after this

### MariaDB Container Env Vars (local only)

| ENV Var Name | Local Value | Remote Value |
| ------------ | ----------- | ------------ |
| ```MARIADB_USER``` | root | N/A |
| ```MARIADB_DATABASE``` | clientreview_db | N/A |
| ```ALLOW_EMPTY_PASSWORD``` | yes | N/A |


# AWS Hosting Info


| SERVICE | OLD | NEW | NOTES |
| ----------- | --- | --- | ----- |
| <tr><td colspan="4"><h3>AWS ECR & ECS -- ELASTIC CONTAINER DOCKER REGISTRY AND SERVICE</h3></td></tr> |
| Docker Registry | tvg/clientreviewtool | tvg/clientreviewtool-apache | Must be AMD docker<br> image, NOT ARM |
| Cluster | TVG-Dockers | Client-Review-Tool-Prod | - |
| Task | tvg-client-review-tool-prod2 | tvg-clientreviewtool-test | - |
| Task Def. | tvg-client-review-tool-prod |tvg-clientreviewtool-apache-test | Uses ENV VARS<br>shown above |
| <tr><td colspan="4"><h3>AWS EC2 SERVER INFO</h3></td></tr> |
| EC2 Server | EC2ContainerService-TVG-Dockers | Client-Review-Tool-Prod | - |
| EC2 IP | 54.242.117.221 | 18.206.208.114 | - |
| SSH Name | tvgecsd | crtecs | Defined in ~/.ssh/config |
| <tr><td colspan="4"><h3>AWS EFS -- ELASTIC FILE SERVER</h3>
| File Server | fs-f4ca85bd | fs-d971d190 | - |
| Volumes | /efs-backup | /efs | - |
| Mappings | /efs-backup/tvg-crt-prod/html<br> :: /var/www/html<br><br>/efs-backup/tvg-crt-prod/nginx/sites-enabled<br> :: /etc/nginx/sites-enabled | /efs/crt-uploaded-files<br> :: /bitnami/drupal/sites/default/files | - |
| <tr><td colspan="4"><h3>AWS RDS -- MARIADB DATABASE</h3>
| Database| tvg-crt-autoscaling.cacpwuqovbo6.us-east-1.rds.amazonaws.com | tvg-crt-autoscaling.cacpwuqovbo6.us-east-1.rds.amazonaws.com | - |
| Table | clientreview_prod_db | STG: clientreview_db<br>PROD: clientreview_prod_db | - |


-----

## Mounting EFS file server on EC2 if needed

To mount:
```
yarn ecs:sh
cd /
sudo mkdir efs # if it doesn't exist
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-d971d190.efs.us-east-1.amazonaws.com:/ efs
```
Verify mount:
```
df -h
```
To unmount:
```
unmount /efs
```

### Set up auto-mount on restart

* Log in to th ECS instance
    * ```yarn ecs:sh```
* Edit the fstab file
    * ```sudo vi /etc/fstab```
* Add the following line (use "j" to go down, then "o" to insert after last line):
    * ```fs-d971d190.efs.us-east-1.amazonaws.com:/ /efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0```
* Save and exit by typing ```<escape>:wq<enter>```
* Test the auto-mount
    * ```sudo mount -fav```

## Copying Uploaded Files from efs-backup to efs

This only applies, while the old instance is still running, and before switching over to the new instance.

* Login to ___current___ (old) 30 CRT ECS server, which mounts both EFS file servers
    * ```ssh tvgecsd```
* Change into files directory
    * ```cd /efs-backup/tvg-crt-prod/html/clientreview/sites/default/files```
* Find newer files and show rsync dry-run
    * ```find -path '*202[45]-*' -print0 | sudo rsync --dry-run -avz --from0 --files-from=- ./ /efs/crt-uploaded-files/```
* Find newer files and actually rsync
    * ```find -path '*202[45]-*' -print0 | sudo rsync -avz --from0 --files-from=- ./ /efs/crt-uploaded-files/```



## Ensuring That File Uploads Can Work

YOU MUST DO THIS EACH TIME YOU COPY OVER FILES FROM THE OLD CRT ```/efs-backup``` to ```/efs```

Drupal won't be able to create file upload, directories or upload files, unless you do the following:

* FILE PERMISSIONS
    * On EC2 server, change folder and file permissions (may take a long time):

```
yarn ecs:sh
sudo chmod -R 775 /efs/crt-uploaded-files
```

* FILE OWNER/GROUP
    * Inside the Docker container, change the folder and file owner and group (may take a long time):

```
yarn ecs:crt:sh
sudo chown -R daemon:www-data /bitnami/drupal/sites/default/files
```

NOTE: if you know, you've only copied over files, in a specific sub-folder, you can modify the above commands to reference only that sub-folder.
